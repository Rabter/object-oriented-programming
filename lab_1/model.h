#ifndef MODEL_H
#define MODEL_H
#include "model_points.h"
#include "model_edges.h"


struct Model
{
    unsigned points_num, edges_num;
    points_ds points;
    edges_ds edges;
};

Model init_model();
void transfer_model(Model &dst, Model &src);
int add_point_to_model(Model &model, Point &point);
int add_edge_to_model(Model &model, Edge &edge);
points_ds get_model_points(Model &model);
edges_ds get_model_edges(Model &model);

int clear_model(Model &model);

void set_points_count(Model &model, unsigned num);
void set_edges_count(Model &model, unsigned num);
unsigned get_points_count(Model &model);
unsigned get_edges_count(Model &model);

#endif // MODEL_H
