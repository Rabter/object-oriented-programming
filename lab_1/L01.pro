#-------------------------------------------------
#
# Project created by QtCreator 2019-03-22T13:20:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = L01
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    2d_view.cpp \
    hidden_edge_cordinates.cpp \
        main.cpp \
        mainwindow.cpp \
    model.cpp \
    list.cpp \
    edge.cpp \
    point.cpp \
    controller.cpp \
    model_transformers.cpp \
    io.cpp \
    2d_draw_functions.cpp \
    draw_edge.cpp \
    doubleentry.cpp \
    pointentry.cpp \
    scan_line.cpp \
    validentry.cpp \
    stream.cpp \
    request.cpp \
    viewparameters.cpp \
    request_types.cpp \
    node.cpp \
    model_edges.cpp \
    model_points.cpp \
    rotators.cpp \
    file_input.cpp \
    file_stream.cpp \
    model_fields_io.cpp \
    model_io.cpp \
    model_frame_io.cpp

HEADERS += \
    2d_view.h \
    hidden_edge_coordinates.h \
        mainwindow.h \
    err.h \
    model.h \
    point.h \
    edge.h \
    list.h \
    request.h \
    controller.h \
    model_transformers.h \
    io.h \
    request_types.h \
    2d_draw_functions.h \
    draw_edge.h \
    scan_line.h \
    viewparameters.h \
    doubleentry.h \
    pointentry.h \
    validentry.h \
    stream.h \
    node.h \
    model_points.h \
    model_edges.h \
    rotators.h \
    file_input.h \
    file_stream.h \
    model_fields_io.h \
    model_io.h \
    model_frame_io.h

FORMS += \
        mainwindow.ui

DISTFILES += \
    cube_by_edges.txt \
    cube_by_points.txt \
    cube.txt
