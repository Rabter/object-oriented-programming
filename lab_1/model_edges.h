#ifndef MODEL_EDGES_H
#define MODEL_EDGES_H

#include "list.h"
#include "edge.h"

typedef List edges_ds;
typedef Node* edges_el;

int add_edge(edges_ds &edges, Edge edge);
Edge *get_edge_data(edges_el edge);
edges_el get_next_edge(edges_el edge);
edges_el get_edge(edges_ds edges, unsigned index);
edges_el get_first_edge(edges_ds edges);
edges_el get_last_edge(edges_ds edges);
bool is_last_edge(edges_ds edges, edges_el edge);
bool is_stopper_edge(edges_ds edges, edges_el edge);
void clear_edges(edges_ds edges);

#endif // MODEL_EDGES_H
