#include <cstring>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "controller.h"
#include "err.h"
#define FOPEN_ERR_TEXT "Не удалось откыть файл"
#define IO_ERR_TEXT "Неверный формат входных данных в файле"
#define MEM_ERR_TEXT "Возникла ошибка при работе с памятью"
#define FEND_ERR_TEXT "Недостаточно входных данных"
#define INTERNAL_ERR_TEXT "Возникла внутренняя ошибка"
#define MODEL_EMPTY_ERR_TEXT "Модель не загружена"
#define DEFAULT_ERR_TEXT "Возникла неизвестная ошибка"
#define VIEWPOINT_DISTANCE 500

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene = new QGraphicsScene(this);
    scene->setSceneRect(0,0, ui->graphicsView->size().width() - 5, ui->graphicsView->size().height() - 5);
    ui->graphicsView->setScene(scene);
    connect(ui->entry_finname, SIGNAL(returnPressed()), this, SLOT(on_btn_build_clicked()));
    connect(ui->entry_dx, SIGNAL(returnPressed()), this, SLOT(on_btn_move_clicked()));
    connect(ui->entry_dy, SIGNAL(returnPressed()), this, SLOT(on_btn_move_clicked()));
    connect(ui->entry_dz, SIGNAL(returnPressed()), this, SLOT(on_btn_move_clicked()));
    connect(ui->entry_kx, SIGNAL(returnPressed()), this, SLOT(on_btn_scale_clicked()));
    connect(ui->entry_ky, SIGNAL(returnPressed()), this, SLOT(on_btn_scale_clicked()));
    connect(ui->entry_kz, SIGNAL(returnPressed()), this, SLOT(on_btn_scale_clicked()));
    connect(ui->entry_center, SIGNAL(returnPressed()), this, SLOT(on_btn_scale_clicked()));
    connect(ui->entry_angle, SIGNAL(returnPressed()), this, SLOT(on_btn_rotate_clicked()));
    connect(ui->entry_foutname, SIGNAL(returnPressed()), this, SLOT(on_btn_save_clicked()));
}

MainWindow::~MainWindow()
{
    delete ui;
    Request request;
    request.type = DESTROY;
    int rc = controller(request);
    handle_error(rc);
}

void MainWindow::on_btn_build_clicked()
{
    Request request;
    request.type = FILE_INPUT;
    set_ir_finname(request.content.input.finname);
    int rc = controller(request);
    if (handle_error(rc))
    {
        request.type = DRAW;
        set_dr_scene(&(request.content.draw.scene));
        set_dr_view_parameters(&(request.content.draw.parameters));
        rc = controller(request);
    }
    handle_error(rc);
}

void MainWindow::on_btn_move_clicked()
{
    double dx, dy, dz;
    int rc1 = ui->entry_dx->get_double(dx);
    int rc2 = ui->entry_dy->get_double(dy);
    int rc3 = ui->entry_dz->get_double(dz);
    if (rc1 == OK && rc2 ==  OK && rc3 == OK)
    {
        Request request;
        request.type = MOVE;
        request.content.move.dx = dx;
        request.content.move.dy = dy;
        request.content.move.dz = dz;
        int rc = controller(request);
        if (handle_error(rc))
        {
            request.type = DRAW;
            set_dr_scene(&(request.content.draw.scene));
            set_dr_view_parameters(&(request.content.draw.parameters));
            rc = controller(request);
        }
        handle_error(rc);
    }
}

void MainWindow::on_btn_scale_clicked()
{
    double xc, yc, zc;
    double kx, ky, kz;
    int rc1 = ui->entry_kx->get_double(kx);
    int rc2 = ui->entry_ky->get_double(ky);
    int rc3 = ui->entry_kz->get_double(kz);
    int rc4 = ui->entry_center->get_point(xc, yc, zc);
    if (rc1 == OK && rc2 ==  OK && rc3 == OK && rc4 == OK)
    {
        Request request;
        request.type = SCALE;
        request.content.scale.kx = kx;
        request.content.scale.ky = ky;
        request.content.scale.kz = kz;
        request.content.scale.xc = xc;
        request.content.scale.yc = yc;
        request.content.scale.zc = zc;
        int rc = controller(request);
        if (handle_error(rc))
        {
            request.type = DRAW;
            set_dr_scene(&(request.content.draw.scene));
            set_dr_view_parameters(&(request.content.draw.parameters));
            rc = controller(request);
        }
        handle_error(rc);
    }
}


void MainWindow::on_btn_rotate_clicked()
{
    double angle;
    if (ui->entry_angle->get_double(angle) == OK)
    {
        Request request;
        request.type = ROTATE;
        request.content.rotate.angle = angle;
        if (ui->rb_ox->isChecked())
            request.content.rotate.asix = RotateRequest::X_ASIX;
        else if (ui->rb_oy->isChecked())
            request.content.rotate.asix = RotateRequest::Y_ASIX;
        else if (ui->rb_oz->isChecked())
            request.content.rotate.asix = RotateRequest::Z_ASIX;
        int rc = controller(request);
        if (handle_error(rc))
        {
            request.type = DRAW;
            set_dr_scene(&(request.content.draw.scene));
            set_dr_view_parameters(&(request.content.draw.parameters));
            rc = controller(request);
        }
        handle_error(rc);
    }
}

void MainWindow::on_btn_save_clicked()
{
    Request request;
    request.type = SAVE;
    set_sr_foutname(request.content.save.foutname);
    int rc = controller(request);
    handle_error(rc);
}

void MainWindow::set_ir_finname(char *name)
{
    strcpy(name, ui->entry_finname->text().toUtf8().data());
}

void MainWindow::set_dr_scene(QGraphicsScene **scene)
{
    *scene = this->scene;
}

void MainWindow::set_sr_foutname(char *name)
{
    strcpy(name, ui->entry_foutname->text().toUtf8().data());
}

void MainWindow::set_dr_view_parameters(ViewParameters *parameters)
{
    parameters->distance = VIEWPOINT_DISTANCE;
    parameters->screen_width = ui->graphicsView->size().width();
    parameters->screen_height = ui->graphicsView->size().height();
}

bool MainWindow::handle_error(int error_code)
{
    bool rc = false;
    switch (error_code)
    {
    case OK:
        ui->label_err->clear();
        rc = true;
        break;
    case ERR_FOPEN:
        ui->label_err->setText(FOPEN_ERR_TEXT);
        break;
    case ERR_IO:
        ui->label_err->setText(IO_ERR_TEXT);
        break;
    case ERR_MEM:
        ui->label_err->setText(MEM_ERR_TEXT);
        break;
    case ERR_FEND:
        ui->label_err->setText(FEND_ERR_TEXT);
        break;
    case ERR_MODEL_EMPTY:
        ui->label_err->setText(MODEL_EMPTY_ERR_TEXT);
        break;
    case ERR_INTERNAL:
        ui->label_err->setText(INTERNAL_ERR_TEXT);
        break;
    default:
        ui->label_err->setText(DEFAULT_ERR_TEXT);
    }
    return rc;
}
