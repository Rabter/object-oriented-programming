#include "viewparameters.h"

double get_screen_width(ViewParameters &parameters)
{
    return parameters.screen_width;
}

double get_screen_height(ViewParameters &parameters)
{
    return parameters.screen_height;
}

double get_view_distance(ViewParameters &parameters)
{
    return parameters.distance;
}
