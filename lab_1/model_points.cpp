#include <cstdlib>
#include "model_points.h"
#include "err.h"

int add_point(points_ds &points, Point &point)
{
    int rc = OK;
    Point *ptr = static_cast<Point*>(malloc(sizeof(Point)));
    if (ptr)
    {
        *ptr = point;
        rc = add_to_list(points, ptr);
        if (rc != OK)
            free(ptr);
    }
    else
        rc = ERR_MEM;
    return rc;
}

Point *get_point_data(points_el point)
{
    return static_cast<Point*>(point->data);
}

points_el get_next_point(points_el point)
{
    return get_next(point);
}

points_el get_point(points_ds points, unsigned index)
{
    return get_by_index(points, index);
}

points_el get_first_point(points_ds points)
{
    return get_head(points);
}

points_el get_last_point(points_ds points)
{
    return get_tail(points);
}

bool is_last_point(points_ds points, points_el &point)
{
    return is_last(points, point);
}

bool is_stopper_point(points_ds points, points_el &point)
{
    return !point;
    (void)points;
}

void clear_points(points_ds points)
{
    free_clean(points);
}

