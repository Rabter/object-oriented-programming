#ifndef POINTENTRY_H
#define POINTENTRY_H

#include "validentry.h"

class PointEntry: public ValidEntry
{
    Q_OBJECT
public:
    PointEntry(QWidget*);
    int get_point(double &x, double &y, double &z);
};

#endif // POINTENTRY_H
