#include <cmath>
#include "model_transformers.h"
#include "rotators.h"
#include "request_types.h"
#include "err.h"

int move_model(Model &model, MoveRequest parameters)
{
    if (get_points_count(model) == 0)
        return ERR_MODEL_EMPTY;
    if (get_dx(parameters) != 0 || get_dy(parameters) != 0 || get_dz(parameters) != 0)
        for (points_el point = get_first_point(get_model_points(model)); !is_stopper_point(get_model_points(model), point); point = get_next_point(point))
        {
            Point *cur = get_point_data(point);
            cur->x += get_dx(parameters);
            cur->y += get_dy(parameters);
            cur->z += get_dz(parameters);
        }
    return OK;
}

int scale_model(Model &model, ScaleRequest parameters)
{
    if (get_points_count(model) == 0)
        return ERR_MODEL_EMPTY;
    if (get_kx(parameters) != 1 || get_ky(parameters) != 1 || get_kz(parameters) != 1)
        for (points_el point = get_first_point(get_model_points(model)); !is_stopper_point(get_model_points(model), point); point = get_next_point(point))
        {
            Point *cur = get_point_data(point);
            cur->x = get_xc(parameters) + (cur->x - get_xc(parameters)) * get_kx(parameters);
            cur->y = get_yc(parameters) + (cur->y - get_yc(parameters)) * get_ky(parameters);
            cur->z = get_zc(parameters) + (cur->z - get_zc(parameters)) * get_kz(parameters);
        }
    return OK;
}

int rotate_model(Model &model, RotateRequest parameters)
{
    const double pi_div_180 = M_PI / 180;
    if (get_points_count(model) == 0)
        return ERR_MODEL_EMPTY;
    double angle = get_angle(parameters);
    if (angle != 0)
    {
        angle *= pi_div_180;
        switch (get_asix(parameters))
        {
        case RotateRequest::X_ASIX:
            rotate_x(model, angle);
            break;
        case RotateRequest::Y_ASIX:
            rotate_y(model, angle);
            break;
        case RotateRequest::Z_ASIX:
            rotate_z(model, angle);
        }
    }
    return OK;
}
