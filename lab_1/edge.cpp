#include "edge.h"

Edge create_edge(unsigned from, unsigned to)
{
    Edge edge;
    edge.from = from;
    edge.to = to;
    return edge;
}
