#ifndef _ERR_H_
#define _ERR_H_

#define OK 0
#define ERR_FOPEN 1
#define ERR_IO 2
#define ERR_MEM 3
#define ERR_FEND 4
#define ERR_INTERNAL 5
#define ERR_MODEL_EMPTY 6

#endif // _ERR_H_
