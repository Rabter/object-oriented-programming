#include "request.h"


RequestContent *get_content(Request &request) {return &(request.content);}
InputRequest *get_ir(RequestContent &content) {return &(content.input);}
DrawRequest *get_dr(RequestContent &content) {return &(content.draw);}
MoveRequest *get_mr(RequestContent &content) {return &(content.move);}
ScaleRequest *get_scr(RequestContent &content) {return &(content.scale);}
RotateRequest *get_rtr(RequestContent &content) {return &(content.rotate);}
SaveRequest *get_svr(RequestContent &content) {return &(content.save);}

RequestType get_request_type(Request &request)
{
    return request.type;
}

InputRequest *get_input_content(Request &request)
{
    RequestContent *content = get_content(request);
    return get_ir(*content);
}

DrawRequest *get_draw_content(Request &request)
{
    RequestContent *content = get_content(request);
    return get_dr(*content);
}

MoveRequest *get_move_content(Request &request)
{
    RequestContent *content = get_content(request);
    return get_mr(*content);
}

ScaleRequest *get_scale_content(Request &request)
{
    RequestContent *content = get_content(request);
    return get_scr(*content);
}

RotateRequest *get_rotate_content(Request &request)
{
    RequestContent *content = get_content(request);
    return get_rtr(*content);
}

SaveRequest *get_save_content(Request &request)
{
    RequestContent *content = get_content(request);
    return get_svr(*content);
}
