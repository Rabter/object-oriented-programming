#include "draw_edge.h"
#include "point.h"
#include "edge.h"
#include "hidden_edge_coordinates.h"
#include "2d_draw_functions.h"
#include "2d_view.h"

void draw_edge(void *scene, Model model, unsigned edge_num, ViewParameters parameters)
{
    double xfrom, yfrom;
    double xto, yto;
    Edge edge = *get_edge_data(get_edge(get_model_edges(model), edge_num));
    Point from = *get_point_data(get_point(get_model_points(model), edge.from));
    Point to = *get_point_data(get_point(get_model_points(model), edge.to));
     /* Если обе точки перед точкой обзора, просто рисуем линию
      * Если одна точка сзади, определяем точку пересечения с плоскостью, которая
      * параллельна экрану и содержит точку обзора, рисуем линию до туда
      * Если обе точки сзади от точки обзора, ничего не рисуем */
    bool from_is_visible = point_is_visible(from, parameters);
    bool to_is_visible = point_is_visible(to, parameters);
    if (from_is_visible || to_is_visible)
    {
        // Будем считать, что from видна, найдем to
        if (!(from_is_visible && to_is_visible)) // Если одна из точек не видна
        {
            if (to_is_visible) // Если видна to, то не видна from, меняем
                swap_points(from, to);
            get_partly_hidden_coordinates(xto, yto, from, to, parameters); // Находим to
        }
        else // to тоже видна
            get_2d_coordinates(xto, yto, to, parameters);

        get_2d_coordinates(xfrom, yfrom, from, parameters); // Находим from
        draw_line(scene, xfrom, yfrom, xto, yto);
    }
}
