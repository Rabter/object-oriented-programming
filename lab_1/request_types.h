#ifndef INPUTREQUEST_H
#define INPUTREQUEST_H

#include <QGraphicsScene>
#include "viewparameters.h"
#define FILENAME_MAXLEN 30

struct InputRequest
{
    char finname[FILENAME_MAXLEN];
};

struct DrawRequest
{
    QGraphicsScene *scene;
    ViewParameters parameters;
};

struct MoveRequest
{
    double dx, dy, dz;
};

struct ScaleRequest
{
    double xc, yc, zc;
    double kx, ky, kz;
};

struct RotateRequest
{
    double angle;
    enum RotationBy {X_ASIX, Y_ASIX, Z_ASIX} asix;
};

struct SaveRequest
{
    char foutname[FILENAME_MAXLEN];
};

double get_dx(MoveRequest &request);
double get_dy(MoveRequest &request);
double get_dz(MoveRequest &request);
double get_kx(ScaleRequest &request);
double get_ky(ScaleRequest &request);
double get_kz(ScaleRequest &request);
double get_xc(ScaleRequest &request);
double get_yc(ScaleRequest &request);
double get_zc(ScaleRequest &request);
double get_angle(RotateRequest &request);
RotateRequest::RotationBy get_asix(RotateRequest &request);

char *get_ir_finname(InputRequest &request);
void *get_dr_scene(DrawRequest &request);
ViewParameters *get_dr_parameters(DrawRequest &request);
char *get_svr_foutname(SaveRequest &request);

#endif
