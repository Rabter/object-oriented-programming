#include <QString>
#include "pointentry.h"
#define DOUBLE_EXP "[+-]?(?:\\d+(?:\\.\\d*)?|\\.\\d+)"

PointEntry::PointEntry(QWidget *parent): ValidEntry(parent)
{
    expression = DOUBLE_EXP ";" DOUBLE_EXP ";" DOUBLE_EXP;
}

int PointEntry::get_point(double &x, double &y, double &z)
{
    if (validate())
    {
        make_correct();
        QStringList input = this->text().split(QRegExp("; ?"));
        x = input[0].toDouble();
        y = input[1].toDouble();
        z = input[2].toDouble();
        return 0;
    }
    else
    {
        make_inorrect();
        return -1;
    }
}
