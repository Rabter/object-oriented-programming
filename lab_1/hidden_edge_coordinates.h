#ifndef HIDDEN_EDGE_COORDINATES_H
#define HIDDEN_EDGE_COORDINATES_H

#include "point.h"
#include "viewparameters.h"

void get_partly_hidden_coordinates(double &xto, double &yto, Point &from, Point &to, ViewParameters parameters);

#endif // HIDDEN_EDGE_COORDINATES_H
