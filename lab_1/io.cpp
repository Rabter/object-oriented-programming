#include <cstdio>
#include "io.h"
#include "model.h"
#include "file_stream.h"
#include "model_io.h"
#include "2d_draw_functions.h"
#include "draw_edge.h"
#include "err.h"

int read_model_from_stream(Model &model, const char *finname)
{
    int rc = OK;
    stream stream_in = open_stream(finname, "r");
    if (!stream_opened(stream_in))
        return ERR_FOPEN;
    Model tmp_model = init_model();
    rc = read_model(tmp_model, stream_in);
    close_stream(stream_in);
    if (rc == OK)
    {
        clear_model(model);
        transfer_model(model, tmp_model);
    }
    return rc;
}

int update_model_frame(void *scene, Model model, ViewParameters parameters)
{
    clear_scene(scene);
    for (unsigned i = 0; i < get_edges_count(model); ++i)
    {
        draw_edge(scene, model, i, parameters);
    }
    return OK;
}

int save_model_to_stream(Model &model, const char *foutname)
{
    if (get_points_count(model) == 0)
        return ERR_MODEL_EMPTY;
    stream stream_out = open_stream(foutname, "w");
    if (!stream_opened(stream_out))
        return ERR_FOPEN;
    int rc = save_model(model, stream_out);
    close_stream(stream_out);
    return rc;
}
