#ifndef MODEL_IO_H
#define MODEL_IO_H

#include "model.h"
#include "stream.h"

int read_model(Model &model, stream stream_in);
int save_model(Model &model, stream stream_out);

#endif // MODEL_IO_H
