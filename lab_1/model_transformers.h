#ifndef MODEL_TRANSFORMERS_H
#define MODEL_TRANSFORMERS_H

#include "model.h"
#include "request_types.h"

int move_model(Model &model, MoveRequest parameters);
int scale_model(Model &model, ScaleRequest parameters);
int rotate_model(Model &model, RotateRequest parameters);

#endif // MODEL_TRANSFORMERS_H
