#ifndef STREAM_H
#define STREAM_H
#include <cstdio>
#define MODE_LENGTH 5

typedef FILE* stream_t;

struct stream
{
    stream_t subject;
    char mode[MODE_LENGTH];
};

void set_subject(stream &s, stream_t subj);
void set_mode(stream &s, const char *mode);
stream_t get_subject(stream &s);
char *get_mode(stream &s);

#endif // STREAM_H
