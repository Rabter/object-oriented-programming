#include <cstring>
#include "stream.h"


void set_subject(stream &s, stream_t subj)
{
    s.subject = subj;
}

void set_mode(stream &s, const char *mode)
{
    strcpy(s.mode, mode);
}

stream_t get_subject(stream &s)
{
    return s.subject;
}

char *get_mode(stream &s)
{
    return s.mode;
}
