#include "hidden_edge_coordinates.h"
#include "scan_line.h"

void get_partly_hidden_coordinates(double &xto, double &yto, Point &from, Point &to, ViewParameters parameters)
{
    double dx, dy, dz;
    get_shifters(dx, dy, dz, from, to);
    get_to_coordinates(xto, yto, from, dx, dy, dz, parameters);
}
