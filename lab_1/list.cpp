#include <cstdlib>
#include "list.h"
#include "err.h"


List init_list()
{
    List list;
    list.head = list.tail = nullptr;
    return list;
}

int add_to_list(List &list, void *data)
{
    int rc = OK;
    Node *el= static_cast<Node*>(malloc(sizeof(Node)));
    if (el)
    {
        el->next = nullptr;
        el->data = data;
        if (list.tail)
            list.tail->next = el;
        else
            list.head = el;
        list.tail = el;
    }
    else
        rc = ERR_MEM;
    return rc;
}

Node *get_head(List &list)
{
    return list.head;
}

Node *get_tail(List &list)
{
    return list.tail;
}

Node *get_by_index(List &list, unsigned i)
{
    Node *wanted = list.head;
    while (wanted && i)
    {
        wanted = wanted->next;
        --i;
    }
    return wanted;
}

bool is_last(List list, Node *el)
{
    return el == list.tail;
}

void free_clean(List &list)
{
    Node *next;
    for (Node *el = list.head; el; el = next)
    {
        next = el->next;
        free(el->data);
        free(el);
    }
    list.head = list.tail = nullptr;
}
