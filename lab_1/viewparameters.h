#ifndef VIEWPARAMETERS_H
#define VIEWPARAMETERS_H

struct ViewParameters
{
    double screen_width, screen_height;
    double distance;
};

double get_screen_width(ViewParameters &parameters);
double get_screen_height(ViewParameters &parameters);
double get_view_distance(ViewParameters &parameters);

#endif // VIEWPARAMETERS_H
