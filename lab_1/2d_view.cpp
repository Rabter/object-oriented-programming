#include "2d_view.h"

bool get_2d_coordinates(double &x, double &y, Point point, ViewParameters parameters)
{
    if (parameters.distance > point.z)
    {
        double distance = get_view_distance(parameters);
        double width = get_screen_width(parameters), height = get_screen_height(parameters);
        x = width / 2 + point.x * distance / (distance - point.z);
        y = height / 2 - point.y * distance / (distance - point.z);
        return true;
    }
    else
        return false;
}

bool point_is_visible(Point point, ViewParameters parameters)
{
    return parameters.distance > point.z;
}
