#ifndef FILE_STREAM_H
#define FILE_STREAM_H

#include "stream.h"


stream open_stream(const char *name, const char *mode);
bool stream_opened(stream &s);
void close_stream(stream &s);

#endif // FILE_STREAM_H
