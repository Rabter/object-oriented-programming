#ifndef D_VIEW_H
#define D_VIEW_H

#include "viewparameters.h"
#include "point.h"

bool get_2d_coordinates(double &x, double &y, Point point, ViewParameters parameters);
bool point_is_visible(Point point, ViewParameters parameters);

#endif // D_VIEW_H
