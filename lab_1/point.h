#ifndef POINT_H
#define POINT_H

struct Point
{
   double x, y, z;
};

Point create_point(double x, double y, double z);
bool compare_points(Point &p1, Point &p2);
void swap_points(Point &p1, Point &p2);

#endif // POINT_H
