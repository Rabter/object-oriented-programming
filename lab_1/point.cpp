#include "point.h"
#define ABS(a) ((a) > 0? (a) : -(a))
#define EPS 1e-7

Point create_point(double x, double y, double z)
{
    Point point;
    point.x = x;
    point.y = y;
    point.z = z;
    return point;
}

bool compare_points(Point &p1, Point &p2)
{
    return ABS(p1.x - p2.x) < EPS && ABS(p1.y - p2.y) < EPS && ABS(p1.z - p2.z) < EPS;
}

void swap_points(Point &p1, Point &p2)
{
    Point buf;
    buf = p1;
    p1 = p2;
    p2 = buf;
}
