#include "io.h"
#include "model.h"
#include "request.h"
#include "model_transformers.h"
#include "err.h"

int controller(Request request)
{
    int rc = OK;
    static Model model = init_model();
    switch (get_request_type(request))
    {
    case FILE_INPUT:
    {
        InputRequest *content = get_input_content(request);
        rc = read_model_from_stream(model, get_ir_finname(*content));
        break;
    }
    case DRAW:
    {
        DrawRequest *content = get_draw_content(request);
        rc = update_model_frame(get_dr_scene(*content), model, *get_dr_parameters(*content));
        break;
    }
    case MOVE:
        rc = move_model(model, *get_move_content(request));
        break;
    case SCALE:
        rc = scale_model(model, *get_scale_content(request));
        break;
    case ROTATE:
        rc = rotate_model(model, *get_rotate_content(request));
        break;
    case SAVE:
    {
        SaveRequest *content = get_save_content(request);
        rc = save_model_to_stream(model, get_svr_foutname(*content));
        break;
    }
    case DESTROY:
        rc = clear_model(model);
        break;
    default:
        rc = ERR_INTERNAL;
    };
    return rc;
}
