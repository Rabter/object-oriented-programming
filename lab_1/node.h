#ifndef NODE_H
#define NODE_H


struct Node
{
    void* data;
    Node *next;
};

Node *get_next(Node *element);

#endif // NODE_H
