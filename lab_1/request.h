#ifndef REQUEST_H
#define REQUEST_H
#include "request_types.h"

enum RequestType {FILE_INPUT, DRAW, MOVE, SCALE, ROTATE, SAVE, DESTROY};
union RequestContent
{
    InputRequest input;
    DrawRequest draw;
    MoveRequest move;
    ScaleRequest scale;
    RotateRequest rotate;
    SaveRequest save;
};

struct Request
{
    RequestType type;
    RequestContent content;
};

RequestType get_request_type(Request &request);

InputRequest *get_input_content(Request &request);
DrawRequest *get_draw_content(Request &request);
MoveRequest *get_move_content(Request &request);
ScaleRequest *get_scale_content(Request &request);
RotateRequest *get_rotate_content(Request &request);
SaveRequest *get_save_content(Request &request);

#endif // REQUEST_H
