#include "model_io.h"
#include "model_frame_io.h"
#include "err.h"

int read_model(Model &model, stream stream_in)
{
    int rc = read_points_info(model, stream_in);
    if (rc == OK)
        rc = read_edges_info(model, stream_in);
    return rc;
}

int save_model(Model &model, stream stream_out)
{
    int rc = write_points_info(model, stream_out);
    if (rc == OK)
        rc = write_edges_info(model, stream_out);
    return rc;
}
