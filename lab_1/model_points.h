#ifndef MODEL_POINTS_H
#define MODEL_POINTS_H

#include "list.h"
#include "point.h"

typedef List points_ds;
typedef Node* points_el;

int add_point(points_ds &points, Point &point);
Point *get_point_data(points_el point);
points_el get_next_point(points_el point);
points_el get_point(points_ds points, unsigned index);
points_el get_first_point(points_ds points);
points_el get_last_point(points_ds points);
bool is_last_point(points_ds points, points_el &point);
bool is_stopper_point(points_ds points, points_el &point);
void clear_points(points_ds points);


#endif // MODEL_POINTS_H
