#ifndef EDGE_H
#define EDGE_H
#include "point.h"

struct Edge
{
    unsigned from, to;
};

Edge create_edge(unsigned from, unsigned to);

#endif // EDGE_H
