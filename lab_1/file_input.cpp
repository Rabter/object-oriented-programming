#include "file_input.h"
#include "err.h"

int read_num(unsigned &buf, stream &stream_in)
{
    int got = fscanf(get_subject(stream_in), "%u", &buf);
    if (got == 1)
        return OK;
    else if (got == 0)
        return ERR_IO;
    else
        return ERR_FEND;
}

int read_point(Point &point, stream &stream_in)
{
    if (fscanf(get_subject(stream_in), "%lf%lf%lf", &(point.x), &(point.y), &(point.z)) == 3)
        return OK;
    else
        return ERR_IO;
}

int read_edge(Edge &edge, stream &stream_in)
{
    if (fscanf(get_subject(stream_in), "%u%u", &(edge.from), &(edge.to)) == 2)
    {
        --edge.from;
        --edge.to;
        return OK;
    }
    else
        return ERR_IO;
}

int write_num(unsigned num, stream &stream_out)
{
    if (fprintf(get_subject(stream_out), "%u\n", num) > 0)
        return OK;
    else
        return ERR_IO;
}

int write_point(Point &point, stream &stream_out)
{
    if (fprintf(get_subject(stream_out), "%lf %lf %lf\n", point.x, point.y, point.z) > 0)
        return OK;
    else
        return ERR_IO;
}

int write_edge(Edge &edge, stream &stream_out)
{
    if (fprintf(get_subject(stream_out), "%u %u\n", edge.from + 1, edge.to + 1) > 0)
        return OK;
    else
        return ERR_IO;
}
