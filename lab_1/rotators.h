#ifndef ROTATORS_H
#define ROTATORS_H

#include "model.h"

void flat_zero_rotation(double &x, double &y, double angle);
void rotate_x(Model &model, double angle);
void rotate_y(Model &model, double angle);
void rotate_z(Model &model, double angle);

#endif // ROTATORS_H
