#include "model_fields_io.h"
#include "file_input.h"
#include "err.h"


int read_points_count(unsigned &count, stream &stream_in)
{
    return read_num(count, stream_in);
}

int read_points(points_ds &points, unsigned points_num, stream &stream_in)
{
    int rc = OK;
    for (unsigned i = 0; i < points_num && rc == OK; ++i)
    {
        Point point;
        rc = read_point(point, stream_in);
        if (rc == OK)
            rc = add_point(points, point);
    }
    if (rc != OK)
        clear_points(points);
    return rc;
}

int read_edges_count(unsigned &count, stream &stream_in)
{
    return read_num(count, stream_in);
}

int read_edges(edges_ds &edges, unsigned edges_num, stream &stream_in)
{
    int rc = OK;
    for (unsigned i = 0; i < edges_num && rc == OK; ++i)
    {
        Edge edge;
        rc = read_edge(edge, stream_in);
        if (rc == OK)
            rc = add_edge(edges, edge);
    }
    if (rc != OK)
        clear_edges(edges);
    return rc;
}

int write_points_count(unsigned &count, stream &stream_out)
{
    return write_num(count, stream_out);
}

int write_points(points_ds &points, stream &stream_out)
{
    int rc = OK;
    for (points_el point = get_first_point(points);
         !is_stopper_point(points, point) && rc == OK; point = get_next_point(point))
    {
        Point *cur = get_point_data(point);
        rc = write_point(*cur, stream_out);
    }
    return rc;
}

int write_edges_count(unsigned &count, stream &stream_out)
{
    return write_num(count, stream_out);
}

int write_edges(edges_ds &edges, stream &stream_out)
{
    int rc = OK;
    for (edges_el edge = get_first_edge(edges);
         !is_stopper_edge(edges, edge) && rc == OK; edge = get_next_edge(edge))
    {
        Edge *cur = get_edge_data(edge);
        rc = write_edge(*cur, stream_out);
    }
    return rc;
}
