#include "scan_line.h"
#include "2d_view.h"
#define MAX(a, b) ((a) > (b)? (a) : (b))
#define ABS(a) ((a) > 0? (a) : -(a))

void get_shifters(double &dx, double &dy, double &dz, Point &from, Point &to)
{
    dx = to.x - from.x;
    dy = to.y - from.y;
    dz = to.z - from.z;
    double l = MAX(MAX(ABS(dx), ABS(dy)), ABS(dz));
    dx /= l;
    dy /= l;
    dz /= l;
}

void get_to_coordinates(double &xto, double &yto, Point &from, double dx, double dy, double dz, ViewParameters &parameters)
{    
    Point point = from, next = from;
    while (point_is_visible(next, parameters))
    {
        point = next;
        next.x += dx;
        next.y += dy;
        next.z += dz;
    }
    get_2d_coordinates(xto, yto, point, parameters);
}
