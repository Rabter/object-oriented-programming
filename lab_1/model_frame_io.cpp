#include "model_frame_io.h"
#include "model_fields_io.h"
#include "err.h"

int read_points_info(Model &model, stream stream_in)
{
    unsigned buf;
    int rc = read_points_count(buf, stream_in);
    if (rc == OK)
    {
        set_points_count(model, buf);
        rc = read_points(model.points, buf, stream_in);
    }
    if (rc != OK)
        clear_points(model.points);
    return rc;
}

int read_edges_info(Model &model, stream stream_in)
{
    unsigned buf;
    int rc = read_edges_count(buf, stream_in);
    if (rc == OK)
    {
        set_edges_count(model, buf);
        rc = read_edges(model.edges, buf, stream_in);
    }
    if (rc != OK)
        clear_edges(model.edges);
    return rc;
}

int write_points_info(Model &model, stream stream_out)
{
    int rc = write_points_count(model.points_num, stream_out);
    if (rc == OK)
        rc = write_points(model.points, stream_out);
    return rc;
}

int write_edges_info(Model &model, stream stream_out)
{
    int rc = write_edges_count(model.edges_num, stream_out);
    if (rc == OK)
        rc = write_edges(model.edges, stream_out);
    return rc;
}
