#ifndef LIST_H
#define LIST_H

#include "node.h"


struct List
{
    Node *head;
    Node *tail;
};

List init_list();
int add_to_list(List &list, void *data);
Node *get_head(List &list);
Node *get_tail(List &list);
Node *get_by_index(List &list, unsigned i);
bool is_last(List list, Node *el);
void free_clean(List &list);

#endif // LIST_H
