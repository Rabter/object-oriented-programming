#ifndef FILE_INPUT_H
#define FILE_INPUT_H

#include "stream.h"
#include "point.h"
#include "edge.h"

int read_num(unsigned &buf, stream &stream_in);
int read_point(Point &point, stream &stream_in);
int read_edge(Edge &edge, stream &stream_in);
int write_num(unsigned num, stream &stream_out);
int write_point(Point &point, stream &stream_out);
int write_edge(Edge &edge, stream &stream_out);

#endif // FILE_INPUT_H
