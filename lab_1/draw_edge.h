#ifndef DRAW_EDGE_H
#define DRAW_EDGE_H

#include "model.h"
#include "viewparameters.h"

void draw_edge(void *scene, Model model, unsigned edge_num, ViewParameters parameters);

#endif // DRAW_EDGE_H
