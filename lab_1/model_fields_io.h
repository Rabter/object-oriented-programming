#ifndef MODEL_FIELDS_IO
#define MODEL_FIELDS_IO

#include "model_points.h"
#include "model_edges.h"
#include "stream.h"

int read_points_count(unsigned &count, stream &stream_in);
int read_points(points_ds &points, unsigned points_num, stream &stream_in);
int read_edges_count(unsigned &count, stream &stream_in);
int read_edges(edges_ds &edges, unsigned edges_num, stream &stream_in);
int write_points_count(unsigned &count, stream &stream_out);
int write_points(points_ds &points, stream &stream_out);
int write_edges_count(unsigned &count, stream &stream_out);
int write_edges(edges_ds &edges, stream &stream_out);

#endif // MODEL_FIELDS_IO
