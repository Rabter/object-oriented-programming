#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "request.h"

int controller(Request request);

#endif // CONTROLLER_H
