#ifndef MODEL_FRAME_INPUT_H
#define MODEL_FRAME_INPUT_H

#include "model.h"
#include "stream.h"

//Input-output of points count and data and edges count and data
int read_points_info(Model &model, stream stream_in); //Running out of fantasy -_-
int read_edges_info(Model &model, stream stream_in);
int write_points_info(Model &model, stream stream_out);
int write_edges_info(Model &model, stream stream_out);

#endif // MODEL_FRAME_INPUT_H
