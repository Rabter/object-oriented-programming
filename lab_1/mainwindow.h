#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include "viewparameters.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btn_build_clicked();

    void on_btn_move_clicked();

    void on_btn_scale_clicked();

    void on_btn_rotate_clicked();

    void on_btn_save_clicked();

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    void set_ir_finname(char *name);
    void set_sr_foutname(char *name);
    void set_dr_scene(QGraphicsScene **scene);
    void set_dr_view_parameters(ViewParameters *parameters);
    bool handle_error(int error_code);
};

#endif // MAINWINDOW_H
