#include "file_stream.h"

stream open_stream(const char *name, const char* mode)
{
    stream s;
    set_subject(s, fopen(name, mode));
    set_mode(s, mode);
    return s;
}

bool stream_opened(stream &s)
{
    return get_subject(s);
}

void close_stream(stream &s)
{
    fclose(get_subject(s));
}
