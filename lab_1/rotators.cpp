#include <cmath>
#include "rotators.h"

void flat_zero_rotation(double &x, double &y, double angle)
{
    double tmp_x = x;
    x = x * cos(angle) + y * sin(angle);
    y = y * cos(angle) - tmp_x * sin(angle);
}

void rotate_x(Model &model, double angle)
{
    for (points_el point = get_first_point(get_model_points(model)); !is_stopper_point(get_model_points(model), point); point = get_next_point(point))
    {
        Point *cur = get_point_data(point);
        flat_zero_rotation(cur->y, cur->z, angle);
    }
}

void rotate_y(Model &model, double angle)
{
    for (points_el point = get_first_point(get_model_points(model)); !is_stopper_point(get_model_points(model), point); point = get_next_point(point))
    {
        Point *cur = get_point_data(point);
        flat_zero_rotation(cur->z, cur->x, angle);
    }
}

void rotate_z(Model &model, double angle)
{
    for (points_el point = get_first_point(get_model_points(model)); !is_stopper_point(get_model_points(model), point); point = get_next_point(point))
    {
        Point *cur = get_point_data(point);
        flat_zero_rotation(cur->x, cur->y, angle);
    }
}
