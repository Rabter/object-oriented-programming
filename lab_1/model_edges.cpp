#include <cstdlib>
#include "model_edges.h"
#include "err.h"

int add_edge(edges_ds &edges, Edge edge)
{
    int rc = OK;
    Edge *ptr = static_cast<Edge*>(malloc(sizeof(Edge)));
    if (ptr)
    {
        *ptr = edge;
        rc = add_to_list(edges, ptr);
    }
    else
        rc = ERR_MEM;
    return rc;
}

Edge *get_edge_data(edges_el edge)
{
    return static_cast<Edge*>(edge->data);
}

edges_el get_next_edge(edges_el edge)
{
    return get_next(edge);
}

edges_el get_edge(edges_ds edges, unsigned index)
{
    return get_by_index(edges, index);
}

edges_el get_first_edge(edges_ds edges)
{
    return get_head(edges);
}

edges_el get_last_edge(edges_ds edges)
{
    return get_tail(edges);
}

bool is_last_edge(edges_ds edges, edges_el edge)
{
    return is_last(edges, edge);
}

bool is_stopper_edge(edges_ds edges, edges_el edge)
{
    return !edge;
    (void)edges;
}

void clear_edges(edges_ds edges)
{
    free_clean(edges);
}
