#ifndef SCAN_LINE_H
#define SCAN_LINE_H

#include "point.h"
#include "viewparameters.h"

void get_shifters(double &dx, double &dy, double &dz, Point &from, Point &to);
void get_to_coordinates(double &xto, double &yto, Point &from, double dx, double dy, double dz, ViewParameters &parameters);

#endif // SCAN_LINE_H
