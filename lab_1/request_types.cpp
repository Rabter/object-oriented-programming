#include "request_types.h"

double get_dx(MoveRequest &request)
{
    return request.dx;
}

double get_dy(MoveRequest &request)
{
    return request.dy;
}

double get_dz(MoveRequest &request)
{
    return request.dz;
}

double get_kx(ScaleRequest &request)
{
    return request.kx;
}

double get_ky(ScaleRequest &request)
{
    return request.ky;
}

double get_kz(ScaleRequest &request)
{
    return request.kz;
}

double get_xc(ScaleRequest &request)
{
    return request.xc;
}

double get_yc(ScaleRequest &request)
{
    return request.yc;
}

double get_zc(ScaleRequest &request)
{
    return request.zc;
}

double get_angle(RotateRequest &request)
{
    return request.angle;
}

RotateRequest::RotationBy get_asix(RotateRequest &request)
{
    return request.asix;
}

char *get_ir_finname(InputRequest &request)
{
    return request.finname;
}

void *get_dr_scene(DrawRequest &request)
{
    return request.scene;
}

ViewParameters *get_dr_parameters(DrawRequest &request)
{
    return &(request.parameters);
}

char *get_svr_foutname(SaveRequest &request)
{
    return request.foutname;
}
