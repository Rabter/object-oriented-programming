#include "model.h"
#include "point.h"
#include "edge.h"
#include "model_points.h"
#include "model_edges.h"
#include "err.h"


Model init_model()
{
    Model model;
    model.edges = init_list();
    model.points = init_list();
    model.edges_num = 0;
    model.points_num = 0;
    return model;
}

void transfer_model(Model &dst, Model &src)
{
    dst = src;
}

int add_point_to_model(Model &model, Point &point)
{
    return add_point(model.points, point);
}

int add_edge_to_model(Model &model, Edge &edge)
{
    return add_edge(model.edges, edge);
}

points_ds get_model_points(Model &model)
{
    return model.points;
}

points_ds get_model_edges(Model &model)
{
    return model.edges;
}

int clear_model(Model &model)
{
    clear_points(model.points);
    clear_edges(model.edges);
    return OK;
}

void set_points_count(Model &model, unsigned num)
{
    model.points_num = num;
}

void set_edges_count(Model &model, unsigned num)
{
    model.edges_num = num;
}

unsigned get_points_count(Model &model)
{
    return model.points_num;
}

unsigned get_edges_count(Model &model)
{
    return model.edges_num;
}
