#ifndef DRAW_FUNCTIONS_H
#define DRAW_FUNCTIONS_H

//void draw_point(void *scene, int x, int y);
void draw_line(void *scene, double x1, double y1, double x2, double y2);
void clear_scene(void *scene);

#endif // DRAW_FUNCTIONS_H
