#ifndef IO_H
#define IO_H

#include "model.h"
#include "viewparameters.h"

int read_model_from_stream(Model &model, const char *finname);
int update_model_frame(void *scene, Model model, ViewParameters parameters);
int save_model_to_stream(Model &model, const char *foutname);

#endif
