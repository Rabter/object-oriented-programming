#include <QGraphicsScene>
#include "2d_draw_functions.h"

void draw_line(void *scene, double x1, double y1, double x2, double y2)
{
    static_cast<QGraphicsScene*>(scene)->addLine(x1, y1, x2, y2);
}

void clear_scene(void *scene)
{
    static_cast<QGraphicsScene*>(scene)->clear();
}
