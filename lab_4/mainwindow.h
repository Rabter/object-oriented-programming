#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <memory>
#include "mediator.h"
#include "drawkit/drawer.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btn_build_clicked();

    void on_btn_add_camera_clicked();

    void on_cb_cameras_currentTextChanged(const QString &arg1);

    void on_cb_objects_currentTextChanged(const QString &arg1);

    void on_btn_save_clicked();

    void on_btn_rotate_clicked();

    void on_btn_scale_clicked();

    void on_btn_move_clicked();

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;

    Mediator mediator;
    std::shared_ptr<Drawkit::Drawer> drawer;
    unsigned availible_module, availible_camera;

    QString object;

    void add_camera();
    void add_module();

    void set_camera(const QString &name);
    void set_object(const QString &name);

    void move();
    void scale();
    void rotate();

    void render();

    void save();
};

#endif // MAINWINDOW_H
