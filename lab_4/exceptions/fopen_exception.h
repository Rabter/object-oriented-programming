#ifndef FOPEN_EXCEPTION_H
#define FOPEN_EXCEPTION_H

#include "namespaces.h"
#include "exceptions/base_exception.h"

namespace Exceptions
{

class FOpenException: public BaseException
{
public:
    FOpenException(): BaseException("File is missing") {}
    FOpenException(const std::string &message, time_t time = time(NULL), unsigned line = 0): BaseException(message, time, line) {}
};

}

#endif // FOPEN_EXCEPTION_H
