#ifndef BASE_EXCEPTION_H
#define BASE_EXCEPTION_H

#include <string>
#include <ctime>
#include "namespaces.h"

namespace Exceptions
{

class BaseException: public std::exception
{
public:
    BaseException(const std::string &message, time_t time = time(NULL), unsigned line = 0):
        msg(message), _time(time), line(line) {}
    virtual const char* what() const noexcept { return msg.c_str(); }
    virtual unsigned where() const { return line; }
    virtual time_t when() const { return _time; }
    virtual const std::string& message() const { return msg; }

    friend std::ostream& operator << (std::ostream &stout, const BaseException &output);

protected:
    std::string msg;
    time_t _time;
    unsigned line;
};

}

#endif // BASE_EXCEPTION_H
