#ifndef MISSING_CAMERA_EXCEPTION_H
#define MISSING_CAMERA_EXCEPTION_H

#include "namespaces.h"
#include "exceptions/base_exception.h"

namespace Exceptions
{

class MissingCameraException: public BaseException
{
public:
    MissingCameraException(): BaseException("Chosen camera is missing") {}
    MissingCameraException(const std::string &message, time_t time = time(NULL), unsigned line = 0): BaseException(message, time, line) {}
};

}

#endif // MISSING_CAMERA_EXCEPTION_H
