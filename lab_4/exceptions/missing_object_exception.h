#ifndef MISSING_OBJECT_EXCEPTION_H
#define MISSING_OBJECT_EXCEPTION_H

#include "namespaces.h"
#include "exceptions/base_exception.h"

namespace Exceptions
{

class MissingObjectException: public BaseException
{
public:
    MissingObjectException(): BaseException("Chosen object is missing") {}
    MissingObjectException(const std::string &message, time_t time = time(NULL), unsigned line = 0): BaseException(message, time, line) {}
};

}

#endif // MISSING_OBJECT_EXCEPTION_H
