#ifndef NAMESPACES_H
#define NAMESPACES_H
// A list of namespaces used in project. All are placed in one file to avoid doublers.

namespace Math {}
namespace Scene
{
    namespace Objects {}
}
namespace SceneManager {}
namespace Drawkit {}
namespace IO {}
namespace Commands {}
namespace Exceptions {}

#endif // NAMESPACES_H
