#include "point_projector_simplified.h"

PointProjectorSimplified::PointProjectorSimplified(const std::shared_ptr<SceneManager::Camera> &cam_ptr, double screen_width, double screen_height):
    PointProjector(cam_ptr)
{
    position = camera->position();
    width = screen_width;
    height = screen_height;
}

bool PointProjectorSimplified::point_is_visible(Math::Point point)
{ return position.z() > point.z(); }

bool PointProjectorSimplified::get_2d_coordinates(double &x, double &y, Math::Point point)
{
    if (point_is_visible(point))
    {
        double distance = position.z();
        x = width / 2 + point.x() * distance / (distance - point.z());
        y = height / 2 - point.y() * distance / (distance - point.z());
        return true;
    }
    else
        return false;
}
