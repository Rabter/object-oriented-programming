#include <QString>
#include "pointentry.h"

PointEntry::PointEntry(QWidget *parent): ValidEntry(parent)
{
    expression = "[+]?\\d+; ?[+-]?\\d+";
}

bool PointEntry::get_point(QPoint &point)
{
    if (validate())
    {
        make_correct();
        QStringList input = this->text().split(QRegExp("; ?"));
        int x = input[0].toInt(), y = input[1].toInt();
        point.setX(x);
        point.setY(y);
        return true;
    }
    else
    {
        make_inorrect();
        return false;
    }
}
