#include <QMessageBox>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "flat_drawer_qt.h"
#include "exceptions/fopen_exception.h"
#include "exceptions/missing_camera_exception.h"
#include "exceptions/missing_object_exception.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);    
    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);
    scene->setSceneRect(0,0, ui->graphicsView->size().width() - 5, ui->graphicsView->size().height() - 5);
    std::shared_ptr<FlatDrawerQt> qt_drawer(new FlatDrawerQt(scene));
    drawer = std::shared_ptr<Drawkit::Drawer>(new Drawkit::Drawer());
    drawer->set_drawer(qt_drawer);
    drawer->set_width(scene->width());
    drawer->set_height(scene->height());
    availible_module = availible_camera = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::add_module()
{
    std::string module_name = "module" + std::to_string(availible_module);
    mediator.add_module(ui->entry_finname->text().toLocal8Bit().data(), module_name);
    ++availible_module;
    ui->cb_objects->addItem(module_name.c_str());
}

void MainWindow::render()
{
    scene->clear();
    mediator.draw(drawer);
}

void MainWindow::save()
{
    mediator.save(ui->entry_foutname->text().toLocal8Bit().data(), object.toLocal8Bit().data());
}

void MainWindow::on_btn_add_camera_clicked()
{
    add_camera();
}

void MainWindow::add_camera()
{
    std::string camera_name = "camera" + std::to_string(availible_camera);
    mediator.add_camera(camera_name);
    ++availible_camera;
    ui->cb_objects->addItem(camera_name.c_str());
    ui->cb_cameras->addItem(camera_name.c_str());
}

void MainWindow::set_camera(const QString &name)
{
    mediator.set_camera(name.toLocal8Bit().data());
    render();
}

void MainWindow::set_object(const QString &name)
{
    object = name;
}

void MainWindow::move()
{
    bool ok1 = ui->entry_dx->validate();
    bool ok2 = ui->entry_dy->validate();
    bool ok3 = ui->entry_dz->validate();
    if (ok1 && ok2 && ok3)
    {
        double dx, dy, dz;
        ui->entry_dx->get_double(dx);
        ui->entry_dy->get_double(dy);
        ui->entry_dz->get_double(dz);
        try
        {
            mediator.shift_object(object.toLocal8Bit().data(), dx, dy, dz);
            render();
        }
        catch(const Exceptions::MissingObjectException)
        {
            QMessageBox::critical(nullptr, "Error", "Select a correct object");
        }
    }
}

void MainWindow::rotate()
{
    double angle;
    if (ui->entry_angle->get_double(angle))
    {
        const double coef = M_PI / 180;
        angle *= coef;
        double ax = 0, ay = 0, az = 0;
        if (ui->rb_ox->isChecked())
            ax = angle;
        else if (ui->rb_oy->isChecked())
            ay = angle;
        else if (ui->rb_oz->isChecked())
            az = angle;
        try
        {
            mediator.rotate_object(object.toLocal8Bit().data(), ax, ay, az);
            render();
        }
        catch(const Exceptions::MissingObjectException)
        {
            QMessageBox::critical(nullptr, "Error", "Select a correct object");
        }
    }
}

void MainWindow::scale()
{
    bool ok1 = ui->entry_kx->validate();
    bool ok2 = ui->entry_ky->validate();
    bool ok3 = ui->entry_kz->validate();
    if (ok1 && ok2 && ok3)
    {
        double kx, ky, kz;
        ui->entry_kx->get_double(kx);
        ui->entry_ky->get_double(ky);
        ui->entry_kz->get_double(kz);
        try
        {
            mediator.scale_object(object.toLocal8Bit().data(), kx, ky, kz);
            render();
        }
        catch(const Exceptions::MissingObjectException)
        {
            QMessageBox::critical(nullptr, "Error", "Select a correct object");
        }
    }
}


void MainWindow::on_btn_build_clicked()
{
    try
    {
        add_module();
        render();
    }
    catch(const Exceptions::FOpenException &err)
    {
        QMessageBox::critical(nullptr, "Error", err.what());
    }
    catch(const Exceptions::MissingCameraException)
    {
        QMessageBox::critical(nullptr, "Error", "Select a correct camera");
    }
}


void MainWindow::on_cb_cameras_currentTextChanged(const QString &arg1)
{
    set_camera(arg1);
}

void MainWindow::on_cb_objects_currentTextChanged(const QString &arg1)
{
    set_object(arg1);
}

void MainWindow::on_btn_save_clicked()
{
    try
    {
        save();
    }
    catch(const Exceptions::FOpenException &err)
    {
        QMessageBox::critical(nullptr, "Error", err.what());
    }
    catch(const Exceptions::MissingObjectException)
    {
        QMessageBox::critical(nullptr, "Error", "Select a correct object");
    }
}

void MainWindow::on_btn_rotate_clicked()
{
    rotate();
}

void MainWindow::on_btn_scale_clicked()
{
    scale();
}

void MainWindow::on_btn_move_clicked()
{
    move();
}
