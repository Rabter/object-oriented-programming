#ifndef POINTENTRY_H
#define POINTENTRY_H

#include "validentry.h"

class PointEntry: public ValidEntry
{
    Q_OBJECT
public:
    PointEntry(QWidget*);
    bool get_point(QPoint &point);
};

#endif // POINTENTRY_H
