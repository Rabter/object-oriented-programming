#ifndef DRAWER_H
#define DRAWER_H

#include <memory>
#include "namespaces.h"
#include "scene/objects/base_drawer.h"
#include "drawkit/flat_drawer.h"
#include "drawkit/point_projector.h"
#include "scene/objects/component.h"

namespace Drawkit
{

class Drawer: public Scene::Objects::BaseDrawer
{
public:
    Drawer() = default;
    void set_drawer(const std::shared_ptr<FlatDrawer> &drawer);
    void set_projector(const std::shared_ptr<PointProjector> &projector);

    void set_width(double width);
    void set_height(double height);

    double width();
    double height();

    void draw(const Scene::Objects::Component &component) const override;

private:
    std::shared_ptr<FlatDrawer> drawer;
    std::shared_ptr<PointProjector> projector;
    void draw_edge(Math::Point from, Math::Point to) const;
    double _width, _height;
};

} // namespace Drawkit

#endif // DRAWER_H
