#include "drawer.h"

namespace Drawkit
{

void Drawer::set_drawer(const std::shared_ptr<FlatDrawer> &drawer)
{ this->drawer = drawer; }

void Drawer::set_projector(const std::shared_ptr<PointProjector> &projector)
{ this->projector = projector; }

void Drawer::set_width(double width)
{ _width = width; }

void Drawer::set_height(double height)
{ _height = height; }

double Drawer::width()
{ return _width; }

double Drawer::height()
{ return _height; }

void Drawer::draw(const Scene::Objects::Component &component) const
{
    auto points = component.get_points();
    auto edges = component.get_edges();
    for (auto edge: edges)
        draw_edge(points[edge.first - 1], points[edge.second - 1]);
}

void Drawer::draw_edge(Math::Point from, Math::Point to) const
{
    double xfrom, yfrom;
    double xto, yto;
    /* Если обе точки перед точкой обзора, просто рисуем линию
    * Если одна точка сзади, определяем точку пересечения с плоскостью, которая
    * параллельна экрану и содержит точку обзора, рисуем линию до туда
    * Если обе точки сзади от точки обзора, ничего не рисуем */
    bool from_is_visible = projector->point_is_visible(from);
    bool to_is_visible = projector->point_is_visible(to);
    if (from_is_visible || to_is_visible)
    {
        // Будем считать, что from видна, найдем to
        if (!(from_is_visible && to_is_visible)) // Если одна из точек не видна
        {
            if (to_is_visible) // Если видна to, то не видна from, меняем
                std::swap(from, to);
            projector->get_partly_hidden_coordinates(xto, yto, from, to); // Находим to
        }
        else // to тоже видна
            projector->get_2d_coordinates(xto, yto, to);

        projector->get_2d_coordinates(xfrom, yfrom, from); // Находим from
        drawer->draw_line(xfrom, yfrom, xto, yto);
    }
}

} // namespace Drawkit
