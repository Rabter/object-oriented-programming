#ifndef POINT_PROJECTOR_H
#define POINT_PROJECTOR_H

#include "namespaces.h"
#include "math/point.h"
#include "scene_manager/camera.h"

namespace Drawkit
{

class PointProjector
{
public:
    PointProjector(const std::shared_ptr<SceneManager::Camera> &cam_ptr);
    virtual bool point_is_visible(Math::Point point) = 0;
    virtual bool get_2d_coordinates(double &x, double &y, Math::Point point) = 0;
    virtual void get_partly_hidden_coordinates(double &xto, double &yto, Math::Point &from, Math::Point &to);

protected:
    std::shared_ptr<SceneManager::Camera> camera;
};

} // namespace Drawkit

#endif // POINT_PROJECTOR_H
