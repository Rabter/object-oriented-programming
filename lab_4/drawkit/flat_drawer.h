#ifndef FLAT_DRAWER_H
#define FLAT_DRAWER_H

#include "namespaces.h"

namespace Drawkit
{

class FlatDrawer
{
public:
    FlatDrawer() = default;
    virtual void draw_line(double x1, double y1, double x2, double y2) = 0;
};

} // namespace Drawkit
#endif // FLAT_DRAWER_H
