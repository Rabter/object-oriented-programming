#include "drawkit/point_projector.h"
#define MAX(a, b) ((a) > (b)? (a) : (b))
#define ABS(a) ((a) > 0? (a) : -(a))

namespace Drawkit
{

PointProjector::PointProjector(const std::shared_ptr<SceneManager::Camera> &cam_ptr): camera(cam_ptr) {}

void PointProjector::get_partly_hidden_coordinates(double &xto, double &yto, Math::Point &from, Math::Point &to)
{
    double dx = to.x() - from.x();
    double dy = to.y() - from.y();
    double dz = to.z() - from.z();
    double l = MAX(MAX(ABS(dx), ABS(dy)), ABS(dz));
    dx /= l;
    dy /= l;
    dz /= l;
    Math::Point point = from, next = from;
    while (point_is_visible(next))
    {
        point = next;
        next.set_x(next.x() + dx);
        next.set_y(next.y() + dy);
        next.set_z(next.z() + dz);
    }
    get_2d_coordinates(xto, yto, point);
}

} // namespace Drawkit
