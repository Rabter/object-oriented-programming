#-------------------------------------------------
#
# Project created by QtCreator 2019-06-15T15:44:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab_4
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    doubleentry.cpp \
    flat_drawer_qt.cpp \
    main.cpp \
    mainwindow.cpp \
    mediator.cpp \
    point_projector_simplified.cpp \
    pointentry.cpp \
    validentry.cpp \
    drawkit/drawer.cpp \
    drawkit/point_projector.cpp \
    math/matrix.cpp \
    math/point.cpp \
    math/transform_matrices.cpp \
    scene/objects/component.cpp \
    scene/objects/model.cpp \
    scene_manager/camera.cpp \
    scene_manager/scene_manager.cpp \
    scene/scene.cpp \
    io/loader.cpp \
    io/saver.cpp \
    io/text_file_input_getter.cpp \
    io/text_file_saver.cpp

HEADERS += \
    doubleentry.h \
    flat_drawer_qt.h \
    mainwindow.h \
    mediator.h \
    namespaces.h \
    point_projector_simplified.h \
    pointentry.h \
    validentry.h \
    drawkit/drawer.h \
    drawkit/flat_drawer.h \
    drawkit/point_projector.h \
    math/matrix.h \
    math/point.h \
    math/transform_matrices.h \
    scene/objects/base_drawer.h \
    scene/objects/component.h \
    scene/objects/model.h \
    scene/objects/object.h \
    scene_manager/camera.h \
    scene_manager/scene_manager.h \
    scene/scene.h \
    io/input_getter.h \
    io/loader.h \
    io/outputter.h \
    io/saver.h \
    io/text_file_input_getter.h \
    io/text_file_saver.h \
    scene/objects/base_saver.h \
    exceptions/base_exception.h \
    exceptions/fopen_exception.h \
    exceptions/missing_camera_exception.h \
    exceptions/missing_object_exception.h

FORMS += \
        mainwindow.ui
