#ifndef MEDIATOR_H
#define MEDIATOR_H

#include <string>
#include <memory>
#include "namespaces.h"
#include "math/matrix.h"
#include "drawkit/drawer.h"
#include "scene_manager/scene_manager.h"
#include "io/loader.h"
#include "io/saver.h"
#include "io/text_file_input_getter.h"
#include "io/text_file_saver.h"

class Mediator
{
public:
    Mediator();

    void add_module(const std::string &file_name, const std::string &module_name);
    void add_camera(const std::string &camera_name);
    void set_camera(const std::string &camera_name);
    void remove_camera(const std::string &camera_name);
    void remove_module(const std::string &module_name);

    void shift_camera(const std::string &camera_name, double dx, double dy, double dz);
    void scale_camera(const std::string &camera_name, double kx, double ky, double kz);
    void rotate_camera(const std::string &camera_name, double ax, double ay, double az);

    void shift_object(const std::string &object_name, double dx, double dy, double dz);
    void scale_object(const std::string &object_name, double kx, double ky, double kz);
    void rotate_object(const std::string &object_name, double ax, double ay, double az);

    void draw(std::shared_ptr<Drawkit::Drawer> &drawer);

    void save(const std::string &file_name, const std::string &object_name);

private:
    std::shared_ptr<SceneManager::SceneManager> scene_manager;
    std::shared_ptr<IO::TextFileInputGetter> getter;
    std::shared_ptr<IO::TextFileSaver> giver;
    std::shared_ptr<IO::Loader> loader;
    std::shared_ptr<IO::Saver> saver;
};

#endif // MEDIATOR_H
