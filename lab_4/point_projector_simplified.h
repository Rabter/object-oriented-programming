#ifndef POINT_PROJECTOR_SIMPLIFIED_H
#define POINT_PROJECTOR_SIMPLIFIED_H

#include <QGraphicsScene>
#include "namespaces.h"
#include "drawkit/point_projector.h"

class PointProjectorSimplified : public Drawkit::PointProjector
{
public:
    PointProjectorSimplified(const std::shared_ptr<SceneManager::Camera> &cam_ptr, double screen_width, double screen_height);
    bool point_is_visible(Math::Point point) override;
    bool get_2d_coordinates(double &x, double &y, Math::Point point) override;
private:
    Math::Point position;
    double width, height;
};

#endif // POINT_PROJECTOR_SIMPLIFIED_H
