#ifndef LOADER_H
#define LOADER_H

#include <memory>
#include "namespaces.h"
#include "io/input_getter.h"
#include "scene/objects/component.h"

namespace IO
{

class Loader
{
public:
    Loader(const std::shared_ptr<InputGetter> &getter);
    std::shared_ptr<Scene::Objects::Component> get_module(const std::string &birthname);

private:
    std::shared_ptr<InputGetter> getter;
};

} // namespace IO


#endif // LOADER_H
