#include "saver.h"

namespace IO
{

Saver::Saver(const std::shared_ptr<Outputter> &giver): giver(giver) {}

void Saver::save_module(const Scene::Objects::Component &module) const
{
    const std::vector<Math::Point> &points = module.get_points();
    std::size_t size = points.size();
    giver->write_count(size);
    for (std::size_t i = 0; i < size; ++i)
        giver->write_point(points[i].x(), points[i].y(), points[i].z());
    const std::vector<std::pair<std::size_t, std::size_t>> &edges = module.get_edges();
    size = edges.size();
    for (std::size_t i = 0; i < size; ++i)
        giver->write_edge(edges[i]);
}

} // namespace IO
