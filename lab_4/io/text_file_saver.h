#ifndef TEXT_FILE_SAVER_H
#define TEXT_FILE_SAVER_H

#include <string>
#include <fstream>
#include "namespaces.h"
#include "io/outputter.h"

namespace IO
{

class TextFileSaver : public Outputter
{
public:
    TextFileSaver() = default;
    TextFileSaver(const std::string &foutname);

    void set_file(const std::string &foutname);

    void write_count(std::size_t count) override;
    void write_point(double x, double y, double z) override;
    void write_edge(const std::pair<std::size_t, std::size_t> &edge) override;

private:
    std::ofstream fout;
};

} // namespace IO

#endif // TEXT_FILE_SAVER_H
