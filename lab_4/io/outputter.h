#ifndef OUTPUTTER_H
#define OUTPUTTER_H

#include <utility>
#include "namespaces.h"

namespace IO
{

class Outputter
{
public:
    virtual void write_count(std::size_t count) = 0;
    virtual void write_point(double x, double y, double z) = 0;
    virtual void write_edge(const std::pair<std::size_t, std::size_t> &edge) = 0;
};

} //namespace IO

#endif // OUTPUTTER_H
