#include "text_file_saver.h"
#include "exceptions/fopen_exception.h"

namespace IO
{

using Exceptions::FOpenException;

TextFileSaver::TextFileSaver(const std::string &foutname): fout(foutname) {}

void TextFileSaver::set_file(const std::string &foutname)
{
    if (fout)
        fout.close();
    fout.open(foutname);
}

void TextFileSaver::write_count(std::size_t count)
{
    if (fout)
        fout << count << std::endl;
    else
        throw(FOpenException("Can not open output file"));
}

void TextFileSaver::write_point(double x, double y, double z)
{
    if (fout)
        fout << x << ' ' << y << ' ' << z << std::endl;
    else
        throw(FOpenException("Can not open output file"));
}

void TextFileSaver::write_edge(const std::pair<std::size_t, std::size_t> &edge)
{
    if (fout)
        fout << edge.first << ' ' << edge.second << std::endl;
    else
        throw(FOpenException("Can not open output file"));
}

} // namespace IO
