#include "text_file_input_getter.h"
#include "exceptions/fopen_exception.h"

namespace IO
{

using Exceptions::FOpenException;

TextFileInputGetter::TextFileInputGetter(const std::string &finname): fin(finname) {}

void TextFileInputGetter::set_file(const std::string &finname)
{
    if (fin)
        fin.close();
    fin.open(finname);
}

std::size_t TextFileInputGetter::read_count()
{
    std::size_t num = 0;
    if (fin)
        fin >> num;
    else
        throw(FOpenException("Can not open input file"));
    return num;
}

std::tuple<double, double, double> TextFileInputGetter::read_point()
{
    double x = 0, y = 0, z = 0;
    if (fin)
        fin >> x >> y >> z;
    else
        throw(FOpenException("Can not open input file"));
    return std::tuple<double, double, double>(x, y, z);
}

std::pair<std::size_t, std::size_t> TextFileInputGetter::read_edge()
{
    std::size_t from = 0, to = 0;
    if (fin)
        fin >> from >> to;
    else
        throw(FOpenException("Can not open input file"));
    return std::pair<std::size_t, std::size_t>(from, to);
}

} // namespace IO

