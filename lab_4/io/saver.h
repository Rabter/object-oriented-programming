#ifndef SAVER_H
#define SAVER_H

#include <memory>
#include <string>
#include "namespaces.h"
#include "io/outputter.h"
#include "scene/objects/component.h"
#include "scene/objects/base_saver.h"

namespace IO
{

class Saver: public Scene::Objects::BaseSaver
{
public:
    Saver(const std::shared_ptr<Outputter> &giver);
    void save_module(const Scene::Objects::Component &module) const override;

private:
    std::shared_ptr<Outputter> giver;
};

} // namespace IO

#endif // SAVER_H
