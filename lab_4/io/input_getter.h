#ifndef INPUT_GETTER_H
#define INPUT_GETTER_H

#include <utility>
#include <tuple>
#include "namespaces.h"

namespace IO
{

class InputGetter
{
public:
    virtual std::size_t read_count() = 0;
    virtual std::tuple<double, double, double> read_point() = 0;
    virtual std::pair<std::size_t, std::size_t> read_edge() = 0;
};

} //namespace IO

#endif // INPUT_GETTER_H
