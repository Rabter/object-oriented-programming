#ifndef TEXT_FILE_INPUT_GETTER_H
#define TEXT_FILE_INPUT_GETTER_H

#include <fstream>
#include "namespaces.h"
#include "input_getter.h"

namespace IO
{

class TextFileInputGetter : public InputGetter
{
public:
    TextFileInputGetter() = default;
    TextFileInputGetter(const std::string &finname);

    void set_file(const std::string &finname);

    std::size_t read_count() override;
    std::tuple<double, double, double> read_point() override;
    std::pair<std::size_t, std::size_t> read_edge() override;

protected:
    std::ifstream fin;
};

} // namespace IO

#endif // TEXT_FILE_INPUT_GETTER_H
