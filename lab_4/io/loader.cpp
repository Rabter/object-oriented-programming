#include "loader.h"
#include "text_file_input_getter.h"
using std::shared_ptr;
using std::tuple;
using std::pair;
using Scene::Objects::Component;

namespace IO
{

Loader::Loader(const shared_ptr<InputGetter> &getter): getter(getter) {}

shared_ptr<Component> Loader::get_module(const std::string &birthname)
{
    shared_ptr<Component> module(new Component(birthname));
    std::size_t points_count = getter->read_count();
    for (std::size_t i = 0; i < points_count; ++i)
    {
        tuple<double, double, double> point = getter->read_point();
        module->add_point(Math::Point(std::get<0>(point), std::get<1>(point), std::get<2>(point)));
    }
    std::size_t edges_count = getter->read_count();
    for (std::size_t i = 0; i < edges_count; ++i)
    {
        pair<std::size_t, std::size_t> edge = getter->read_edge();
        module->add_edge(edge);
    }
    return module;
}

} //namespace IO
