#ifndef FLAT_DRAWER_QT_H
#define FLAT_DRAWER_QT_H

#include <QGraphicsScene>
#include "namespaces.h"
#include "drawkit/flat_drawer.h"

class FlatDrawerQt : public Drawkit::FlatDrawer
{
public:
    FlatDrawerQt(QGraphicsScene *scene = nullptr, const QPen &pen = QPen(Qt::black));
    void set_scene(QGraphicsScene *scene);
    void set_pen(const QPen &pen);

    void draw_line(double x1, double y1, double x2, double y2) override;

private:
    QGraphicsScene *scene;
    QPen pen;
};

#endif // FLAT_DRAWER_QT_H
