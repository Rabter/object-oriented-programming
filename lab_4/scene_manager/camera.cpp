#include "camera.h"

namespace SceneManager
{

Camera::Camera(const std::string &name, double x, double y, double z): Object(name), _position(x, y, z) {}

void Camera::transform(const std::shared_ptr<Math::Matrix> transformer)
{ _position.transform(transformer); }

const Math::Point Camera::position() const
{ return _position; }

} // namespace SceneManager
