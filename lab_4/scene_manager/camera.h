#ifndef CAMERA_H
#define CAMERA_H

#include "namespaces.h"
#include "scene/objects/object.h"
#include "math/point.h"
#define CAMERA_DEFAULT_X 0
#define CAMERA_DEFAULT_Y 0
#define CAMERA_DEFAULT_Z 500

namespace SceneManager
{

class Camera: public Scene::Objects::Object // Or inherit from Componen to make cameras visible
{
public:
    Camera(const std::string &name, double x = CAMERA_DEFAULT_X, double y = CAMERA_DEFAULT_Y, double z = CAMERA_DEFAULT_Z);
    void transform(const std::shared_ptr<Math::Matrix> transformer) override;
    const Math::Point position() const;

private:
    Math::Point _position;
    // To rotate camera by its own asix, add angles here and handlers in drawer
};

} // namespace SceneManager


#endif // CAMERA_H
