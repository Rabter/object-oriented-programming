#ifndef SCENE_MANAGER_H
#define SCENE_MANAGER_H

#include <memory>
#include "namespaces.h"
#include "scene/scene.h"
#include "scene_manager/camera.h"

namespace SceneManager
{

class SceneManager
{
public:
    SceneManager();
    const std::shared_ptr<Scene::Scene>& scene();

    void add_camera(const std::string &name);
    void add_object(const std::shared_ptr<Scene::Objects::Object> &object_ptr);

    void set_view_camera(const std::string &name);
    std::shared_ptr<Camera> get_view_camera();

    std::shared_ptr<Camera>& get_camera(const std::string &name);

    const std::vector<std::shared_ptr<Scene::Objects::Object>>& get_objects();
    const std::shared_ptr<Scene::Objects::Object> object_named(const std::string &name);

    void transform(const std::string &name, const std::shared_ptr<Math::Matrix> &transformer);

    void remove_camera(const std::string &name);
    void remove_object(const std::string &name);


private:
    std::shared_ptr<Scene::Scene> _scene;
    std::vector<std::shared_ptr<Camera>> cameras;
    std::size_t view_camera_index;
};

} //namespace SceneManager

#endif // SCENE_MANAGER_H
