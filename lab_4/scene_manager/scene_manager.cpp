#include "scene_manager.h"
#include "exceptions/missing_object_exception.h"

namespace SceneManager
{

SceneManager::SceneManager(): _scene(new Scene::Scene), view_camera_index(0) {}

const std::shared_ptr<Scene::Scene> &SceneManager::scene()
{ return _scene; }

void SceneManager::add_camera(const std::string &name)
{
    // To avoid cameras with the same name, add name corrector here
    cameras.push_back(std::shared_ptr<Camera>(new Camera(name)));
}

void SceneManager::add_object(const std::shared_ptr<Scene::Objects::Object> &object_ptr)
{ _scene->add_object(object_ptr); }

void SceneManager::set_view_camera(const std::string &name)
{
    bool found = false;
    for (std::size_t i = 0; i < cameras.size() && !found; ++i)
    {
        if (cameras[i]->name() == name)
        {
            view_camera_index = i;
            found = true;
        }
    }
    if (!found)
        throw (0); //ToDo after exceptions are completed
}

std::shared_ptr<Camera> SceneManager::get_view_camera()
{
    if (view_camera_index >= 0 && view_camera_index < cameras.size()) // better keep >= as index's type may be changed later
            return cameras[view_camera_index];
    else
        return nullptr;
}

std::shared_ptr<Camera>& SceneManager::get_camera(const std::string &name)
{
    bool found = false;
    for (std::size_t i = 0; i < cameras.size() && !found; ++i)
        if (cameras[i]->name() == name)
            return cameras[i];
    throw (0); //ToDo after exceptions are completed
}

const std::vector<std::shared_ptr<Scene::Objects::Object> > &SceneManager::get_objects()
{ return _scene->get_objects(); }

const std::shared_ptr<Scene::Objects::Object> SceneManager::object_named(const std::string &name)
{ return _scene->object_named(name); }

void SceneManager::transform(const std::string &name, const std::shared_ptr<Math::Matrix> &transformer)
{
    std::size_t index = _scene->index_of(_scene->object_named(name));
    if (index < _scene->objects_number())
        _scene->transform(index, transformer);
    else
        throw(Exceptions::MissingObjectException());
}

void SceneManager::remove_camera(const std::string &name)
{
    bool found = false;
    for (std::size_t i = 0; i < cameras.size() && !found; ++i)
    {
        if (cameras[i]->name() == name)
        {
            found = true;
            cameras.erase(cameras.cbegin() + i);
            if (view_camera_index >= i)
                --view_camera_index;
        }
    }
    if (!found)
        throw (0); //ToDo after exceptions are completed
}

void SceneManager::remove_object(const std::string &name)
{ _scene->remove_object(_scene->object_named(name)); }

} // namespace SceneManager
