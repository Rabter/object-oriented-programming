#include "component.h"

namespace Scene
{
namespace Objects
{

Component::Component(std::string name): Object(name), points(0), edges(0) {}

void Component::add_point(const Math::Point &point)
{ points.push_back(point); }

void Component::add_edge(const Edge &edge)
{ edges.push_back(edge); }

void Component::transform(const std::shared_ptr<Math::Matrix> transformer)
{
    for (Math::Point &current : points)
        current.transform(transformer);
}

void Component::draw(const BaseDrawer &drawer)
{ drawer.draw(*this); }

void Component::save(const BaseSaver &saver)
{ saver.save_module(*this); }

const std::vector<Math::Point> &Component::get_points() const
{ return points; }

const std::vector<Component::Edge> &Component::get_edges() const
{ return edges; }

} // namespace Objects
} // namespace Scene
