#ifndef BASE_DRAWER_H
#define BASE_DRAWER_H

#include "namespaces.h"

namespace Scene
{
namespace Objects
{

class Component;

class BaseDrawer
{
public:
    virtual void draw(const Component &component) const = 0;
};

}
}

#endif // BASE_DRAWER_H
