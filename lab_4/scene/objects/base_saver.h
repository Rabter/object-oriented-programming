#ifndef BASE_SAVER_H
#define BASE_SAVER_H

#include "namespaces.h"

namespace Scene
{
namespace Objects
{

class Component;

class BaseSaver
{
public:
    virtual void save_module(const Component &module) const = 0;
};

}
}

#endif // BASE_SAVER_H
