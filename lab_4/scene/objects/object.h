#ifndef OBJECT_H
#define OBJECT_H

#include <vector>
#include <string>
#include "namespaces.h"
#include "math/point.h"
#include "scene/objects/base_drawer.h"
#include "scene/objects/base_saver.h"

namespace Scene
{
namespace Objects
{

class Object
{
public:
    Object(const std::string &name): _name(name) {}
    virtual void transform(const std::shared_ptr<Math::Matrix> transformer) = 0;
    virtual void draw(const BaseDrawer &drawer) { (void)drawer; }
    virtual void save(const BaseSaver &saver) { (void)saver; }
    const std::string& name() { return _name; }
protected:
    std::string _name;
};

} // namespace Objects
} //namespace Scene

#endif // OBJECT_H
