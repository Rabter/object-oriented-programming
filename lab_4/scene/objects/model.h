#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include <memory>
#include "namespaces.h"
#include "object.h"

namespace Scene
{
namespace Objects
{

class Model : public Object
{
public:
    Model(std::string name);
    std::size_t add_object(std::shared_ptr<Object> ptr);
    std::size_t index_of(std::shared_ptr<Object> ptr);
    void remove(std::size_t index);

    void transform(const std::shared_ptr<Math::Matrix> transformer) override;

private:
    std::vector<std::shared_ptr<Object>> components;
};

} // namespace Objects
} // namespace Scene


#endif // MODEL_H
