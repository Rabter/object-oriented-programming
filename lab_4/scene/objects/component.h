#ifndef COMPONENT_H
#define COMPONENT_H

#include <vector>
#include "namespaces.h"
#include "math/point.h"
#include "object.h"

namespace Scene
{
namespace Objects
{

class Component : public Object
{
public:
    typedef std::pair<size_t, size_t> Edge;
    Component(std::string name);
    void add_point(const Math::Point &point);
    void add_edge(const Edge &edge);
    void transform(const std::shared_ptr<Math::Matrix> transformer) override;
    void draw(const BaseDrawer &drawer) override;
    void save(const BaseSaver &saver) override;

    const std::vector<Math::Point>& get_points() const;
    const std::vector<Edge>& get_edges() const;

private:
    std::vector<Math::Point> points;
    std::vector<Edge> edges;
};

} // namespace Objects
} // namespace Scene

#endif // COMPONENT_H
