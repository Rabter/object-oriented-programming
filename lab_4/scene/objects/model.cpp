#include "model.h"

namespace Scene
{
namespace Objects
{

Model::Model(std::string name): Object(name), components(0) {}

std::size_t Model::add_object(std::shared_ptr<Object> ptr)
{
    components.push_back(ptr);
    return components.size() - 1;
}

std::size_t Model::index_of(std::shared_ptr<Object> ptr)
{
    std::size_t size = components.size();
    for (std::size_t i = 0; i < size; ++i)
        if (components[i] == ptr)
            return i;
    return size; // Returns out-of-range if not found
}

void Model::remove(std::size_t index)
{ components.erase(components.cbegin() + index); }

void Model::transform(const std::shared_ptr<Math::Matrix> transformer)
{
    for (auto &component: components)
        component->transform(transformer);
}


} // namespace Objects
} // namespace Scene
