#include "scene.h"

namespace Scene
{

Scene::Scene(): objects(0) {}

std::size_t Scene::add_object(const std::shared_ptr<Objects::Object> &ptr)
{
    objects.push_back(ptr);
    return objects.size() - 1;
}

std::size_t Scene::objects_number()
{ return objects.size(); }

std::size_t Scene::index_of(const std::shared_ptr<Objects::Object> &ptr)
{
    std::size_t size = objects.size();
    for (std::size_t i = 0; i < size; ++i)
        if (objects[i] == ptr)
            return i;
    return size; // Returns out-of-range if not found
}

std::shared_ptr<Objects::Object> Scene::object_named(const std::string &name)
{
    for (std::size_t i = 0; i < objects.size(); ++i)
        if (objects[i]->name() == name)
            return objects[i];
    return nullptr;
}

void Scene::transform(std::size_t index, std::shared_ptr<Math::Matrix> transformer)
{ objects[index]->transform(transformer); }

const std::vector<std::shared_ptr<Objects::Object> > &Scene::get_objects()
{ return objects; }

void Scene::remove_object(const std::shared_ptr<Objects::Object> &ptr)
{
    std::size_t size = objects.size();
    for (std::size_t i = 0; i < size; ++i)
        if (objects[i] == ptr)
            objects.erase(objects.cbegin() + i);
}

void Scene::remove_object(std::size_t index)
{ objects.erase(objects.cbegin() + index); }

const std::shared_ptr<Objects::Object> &Scene::operator [](std::size_t index)
{ return objects[index]; }


} // namespace Scene
