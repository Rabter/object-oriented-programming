#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include "namespaces.h"
#include "objects/object.h"

namespace Scene
{

class Scene
{
public:
    Scene();

    std::size_t add_object(const std::shared_ptr<Objects::Object> &ptr);

    std::size_t objects_number();
    std::size_t index_of(const std::shared_ptr<Objects::Object> &ptr);
    std::shared_ptr<Objects::Object> object_named(const std::string &name);

    void transform(std::size_t index, std::shared_ptr<Math::Matrix> transformer);

    const std::vector<std::shared_ptr<Objects::Object>>& get_objects();

    void remove_object(const std::shared_ptr<Objects::Object> &ptr);
    void remove_object(std::size_t index);

    const std::shared_ptr<Objects::Object>& operator [] (std::size_t index);

private:
    std::vector<std::shared_ptr<Objects::Object>> objects;
};

} // namespace Scene

#endif // SCENE_H
