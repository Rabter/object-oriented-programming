#ifndef VALIDENTRY_H
#define VALIDENTRY_H

#include <QLineEdit>

class ValidEntry : public QLineEdit
{
    Q_OBJECT
public:
    ValidEntry(QWidget *parent);
    virtual int validate();
    virtual void set_reg_exp(QString exp);
protected:
    short valid;    
    QString expression;
    virtual void make_inorrect();
    virtual void make_correct();
protected slots:
    void reset_validation();
};

#endif // VALIDENTRY_H
