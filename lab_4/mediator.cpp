#include "mediator.h"
#include "math/transform_matrices.h"
#include "point_projector_simplified.h"
#include "exceptions/missing_camera_exception.h"
#include "exceptions/missing_object_exception.h"

Mediator::Mediator():
    scene_manager(new SceneManager::SceneManager),
    getter(new IO::TextFileInputGetter),
    giver(new IO::TextFileSaver),
    loader(new IO::Loader(getter)),
    saver(new IO::Saver(giver)) {}

void Mediator::add_module(const std::string &file_name, const std::string &module_name)
{
    getter->set_file(file_name);
    std::shared_ptr<Scene::Objects::Component> module = loader->get_module(module_name);
    scene_manager->add_object(module);
}

void Mediator::add_camera(const std::string &camera_name)
{ scene_manager->add_camera(camera_name); }

void Mediator::set_camera(const std::string &camera_name)
{ scene_manager->set_view_camera(camera_name); }

void Mediator::remove_camera(const std::string &camera_name)
{ scene_manager->remove_camera(camera_name); }

void Mediator::remove_module(const std::string &module_name)
{ scene_manager->remove_object(module_name); }

void Mediator::shift_camera(const std::string &camera_name, double dx, double dy, double dz)
{
    std::shared_ptr<Math::MoveMatrix> shifter(new Math::MoveMatrix(dx, dy, dz));
    scene_manager->get_camera(camera_name)->transform(shifter);
}

void Mediator::scale_camera(const std::string &camera_name, double kx, double ky, double kz)
{
    std::shared_ptr<Math::ScaleMatrix> scaler(new Math::ScaleMatrix(kx, ky, kz));
    scene_manager->get_camera(camera_name)->transform(scaler);
}

void Mediator::rotate_camera(const std::string &camera_name, double ax, double ay, double az)
{
    std::shared_ptr<SceneManager::Camera> camera = scene_manager->get_camera(camera_name);
    std::shared_ptr<Math::RotateOxMatrix> rotator(new Math::RotateOxMatrix(ax));
    camera->transform(rotator);
    rotator = std::shared_ptr<Math::RotateOxMatrix>(new Math::RotateOxMatrix(ay));
    camera->transform(rotator);
    rotator = std::shared_ptr<Math::RotateOxMatrix>(new Math::RotateOxMatrix(az));
    camera->transform(rotator);
}

void Mediator::shift_object(const std::string &object_name, double dx, double dy, double dz)
{
    std::shared_ptr<Math::MoveMatrix> shifter(new Math::MoveMatrix(dx, dy, dz));
    scene_manager->transform(object_name, shifter);
}

void Mediator::scale_object(const std::string &object_name, double kx, double ky, double kz)
{
    std::shared_ptr<Math::ScaleMatrix> scaler(new Math::ScaleMatrix(kx, ky, kz));
    scene_manager->transform(object_name, scaler);
}

void Mediator::rotate_object(const std::string &object_name, double ax, double ay, double az)
{
    std::shared_ptr<Math::Matrix> rotator(new Math::RotateOxMatrix(ax));
    scene_manager->transform(object_name, rotator);
    rotator = std::shared_ptr<Math::Matrix>(new Math::RotateOyMatrix(ay));
    scene_manager->transform(object_name, rotator);
    rotator = std::shared_ptr<Math::Matrix>(new Math::RotateOzMatrix(az));
    scene_manager->transform(object_name, rotator);
}

void Mediator::draw(std::shared_ptr<Drawkit::Drawer> &drawer)
{
    std::shared_ptr<SceneManager::Camera> camera = scene_manager->get_view_camera();
    if (camera == nullptr)
        throw(Exceptions::MissingCameraException());
    std::shared_ptr<PointProjectorSimplified> projector(new PointProjectorSimplified(camera, drawer->width(), drawer->height()));
    drawer->set_projector(projector);
    const std::vector<std::shared_ptr<Scene::Objects::Object>> &objects = scene_manager->get_objects();
    for (auto object: objects)
        object->draw(*drawer);
}

void Mediator::save(const std::string &file_name, const std::string &object_name)
{
    giver->set_file(file_name);
    std::shared_ptr<Scene::Objects::Object> object = scene_manager->object_named(object_name);
    if (object == nullptr)
        throw(Exceptions::MissingObjectException());
    object->save(*saver);
}
