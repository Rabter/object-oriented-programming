#ifndef POINT_H
#define POINT_H

#include <memory>
#include "namespaces.h"
#include "matrix.h"

namespace Math
{

class Point
{
public:
    Point();
    Point(double x, double y, double z);

    double x() const;
    double y() const;
    double z() const;

    void set(double x, double y, double z);
    void set_x(double x);
    void set_y(double y);
    void set_z(double z);

    void transform(const std::shared_ptr<Math::Matrix> matrix);

private:
    double _x, _y, _z;
};

}

#endif // POINT_H
