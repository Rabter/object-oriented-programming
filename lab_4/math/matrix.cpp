#include "matrix.h"

namespace Math
{

Matrix::Matrix(std::size_t rows, std::size_t columns): data(rows)
{
    for (std::vector<double> &row: data)
        row.resize(columns);
}

const std::vector<double>& Matrix::operator [] (std::size_t i) const
{
    return data[i];
}

double Matrix::get(std::size_t i, std::size_t j) const
{
    return data[i][j];
}

void Matrix::set(std::size_t i, std::size_t j, double value)
{
    data[i][j] = value;
}

} // namespace Math
