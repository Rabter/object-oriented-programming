#ifndef MATRIX_H
#define MATRIX_H

#include <vector>
#include "namespaces.h"

namespace Math
{

class Matrix
{
public:
    Matrix() = default;
    Matrix(std::size_t rows, std::size_t columns);

    const std::vector<double>& operator [] (std::size_t i) const;
    double get(std::size_t i, std::size_t j) const;
    void set(std::size_t i, std::size_t j, double value);

protected:
    std::vector<std::vector<double>> data;
};

}

#endif // MATRIX_H
