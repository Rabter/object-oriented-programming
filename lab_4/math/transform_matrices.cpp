#include <cmath>
#include "transform_matrices.h"

namespace Math {

MoveMatrix::MoveMatrix(int x, int y, int z): Matrix(4, 4)
{
    data[0][0] = 1;
    data[1][1] = 1;
    data[2][2] = 1;
    data[3][3] = 1;
    data[0][3] = x;
    data[1][3] = y;
    data[2][3] = z;
}

ScaleMatrix::ScaleMatrix(double x, double y, double z): Matrix(4, 4)
{
    data[0][0] = x;
    data[1][1] = y;
    data[2][2] = z;
    data[3][3] = 1;
}

RotateOxMatrix::RotateOxMatrix(double angle): Matrix(4, 4)
{
    data[0][0] = 1;
    data[1][1] = cos(angle);
    data[1][2] = -sin(angle);
    data[2][1] = sin(angle);
    data[2][2] = cos(angle);
    data[3][3] = 1;
}

RotateOyMatrix::RotateOyMatrix(double angle): Matrix(4, 4)
{
    data[0][0] = cos(angle);
    data[1][1] = 1;
    data[2][0] = -sin(angle);
    data[0][2] = sin(angle);
    data[2][2] = cos(angle);
    data[3][3] = 1;
}

RotateOzMatrix::RotateOzMatrix(double angle): Matrix(4, 4)
{
    data[0][0] = cos(angle);
    data[0][1] = -sin(angle);
    data[1][0] = sin(angle);
    data[1][1] = cos(angle);
    data[2][2] = 1;
    data[3][3] = 1;
}

} // namespace Math
