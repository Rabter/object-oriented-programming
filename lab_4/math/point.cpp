#include "point.h"

namespace Math
{

Point::Point(): _x(0), _y(0), _z(0) {}

Point::Point(double x, double y, double z): _x(x), _y(y), _z(z) {}

double Point::x() const
{
    return _x;
}

double Point::y() const
{
    return _y;
}

double Point::z() const
{
    return _z;
}

void Point::set(double x, double y, double z)
{
    _x = x;
    _y = y;
    _z = z;
}

void Point::set_x(double x) { _x = x; }

void Point::set_y(double y) { _y = y; }

void Point::set_z(double z) { _z = z; }

void Point::transform(const std::shared_ptr<Matrix> matrix)
{
    std::vector<double> result(4, 0.0);
    const std::vector<double> data = {_x, _y, _z, 1.0};
    for (size_t i = 0; i < 4; i++)
        for (size_t j = 0; j < 4; j++)
            result[i] += data[j] * matrix->get(i, j);

    this->set(result[0], result[1], result[2]);
}

} // namespace Math
