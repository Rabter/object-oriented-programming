#include "flat_drawer_qt.h"

FlatDrawerQt::FlatDrawerQt(QGraphicsScene *scene, const QPen &pen): scene(scene), pen(pen) {}

void FlatDrawerQt::set_scene(QGraphicsScene *scene)
{ this->scene = scene; }

void FlatDrawerQt::set_pen(const QPen &pen)
{ this->pen = pen; }

void FlatDrawerQt::draw_line(double x1, double y1, double x2, double y2)
{ scene->addLine(x1, y1, x2, y2, pen); }
