#include "elevatorcabin.h"
#include "time_intervals.h"
#include "int_to_string.h"

ElevatorCabin::ElevatorCabin(QObject *parent, unsigned floor): QObject(parent)
{
    change_state(STILL);
    timer.setSingleShot(true);
    this->floor = floor;
    connect(&(this->timer), SIGNAL(timeout()), this, SLOT(expect()));
}

void ElevatorCabin::stop()
{
    change_state(STILL);
    emit stopped_at(floor);
    emit opening_request();
}

void ElevatorCabin::move_up()
{
    change_state(MOVING_UP);
    timer.start(RISING_TIME);
}

void ElevatorCabin::move_down()
{
    change_state(MOVING_DOWN);
    timer.start(DESCENT_TIME);
}

void ElevatorCabin::expect()
{
    if (state == MOVING_UP)
    {
        change_state(EXPECTING_COMMAND);
        ++floor;
        log_floor();
        emit visited_floor(floor);
    }
    else if (state == MOVING_DOWN)
    {
        change_state(EXPECTING_COMMAND);
        --floor;
        log_floor();
        emit visited_floor(floor);
    }
    // Any other state must not emit 'visited_floor'
}

void ElevatorCabin::log_all()
{
    log_floor();
    log_state();
}

void ElevatorCabin::change_state(ElevatorCabin::CabinState new_state)
{
    if (state != new_state)
    {
        state = new_state;
        if (state != EXPECTING_COMMAND)
            log_state();
    }
}

void ElevatorCabin::log_state()
{
    if (state != EXPECTING_COMMAND)
    {
        QString state_str("Lift is ");
        if (state == STILL)
            state_str += "standing still";
        else if (state == MOVING_UP)
            state_str += "moving up";
        else if (state == MOVING_DOWN)
            state_str += "moving down";
        emit log_request(state_str);
    }
}

void ElevatorCabin::log_floor()
{
    QString txt("Current floor: ");
    txt += to_qstring(floor + 1);
    emit log_request(txt);
}
