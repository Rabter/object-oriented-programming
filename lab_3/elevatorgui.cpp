#include <QFrame>
#include <QGroupBox>
#include <QString>
#include "int_to_string.h"
#include "elevatorgui.h"
#include "elevator_gui_geometry.h"

ElevatorGUI::ElevatorGUI(QWidget *parent, unsigned floors_number, int lower): QWidget(parent)
{
    int w = parent->width(), h = parent->height();
    setGeometry(0, 0, w, h);
    QRect form(int(w * REL_HOR_GB_SHIFT), h * REL_VER_GB_SHIFT, w * (0.5 - 1.5 * REL_HOR_GB_SHIFT), h * (1 - 2 * REL_VER_GB_SHIFT));
    create_outside_buttons(floors_number, lower, form);
    form = QRect(int(w * 0.5 * (1 + REL_HOR_GB_SHIFT)), h * REL_VER_GB_SHIFT, w * (0.5 - 1.5 * REL_HOR_GB_SHIFT), h * (1 - 2 * REL_VER_GB_SHIFT));
    create_inside_buttons(floors_number, lower, form);
    QFrame *line = new QFrame(parent);
    line->setObjectName(QString::fromUtf8("line"));
    line->setGeometry(QRect(w / 2, h * REL_VER_GB_SHIFT, 2, h * (1 - 2 * REL_VER_GB_SHIFT)));
    line->setFrameShape(QFrame::VLine);
    line->setFrameShadow(QFrame::Sunken);
}

ElevatorButton* ElevatorGUI::get_inside_buttons()
{
    return inside_buttons;
}

ElevatorButton* ElevatorGUI::get_outside_buttons_up()
{
    return outside_buttons_up;
}

ElevatorButton* ElevatorGUI::get_outside_buttons_down()
{
    return outside_buttons_down;
}

void ElevatorGUI::activate(unsigned index)
{
    inside_buttons[index].setEnabled(true);
    outside_buttons_up[index].setEnabled(true);
    outside_buttons_down[index].setEnabled(true);
}

static inline QString inside_button_text(int i)
{
    return to_qstring(i);
}

static inline QString outside_button_text_up(int i)
{
    return to_qstring(i) + QString("^");
}

static inline QString outside_button_text_down(int i)
{
    return to_qstring(i)+ QString("v");
}

void ElevatorGUI::create_inside_buttons(unsigned floors_numbers, int lower, const QRect &form)
{
    gb_inside_buttons = create_buttons_block(this->parentWidget(), form, "gb_inside_buttons", "Buttons inside cabin");
    QRect form_inside(0, 0, form.width(), form.height());
    inside_buttons = create_buttons(gb_inside_buttons, floors_numbers, lower, form_inside, inside_button_text);
}

void ElevatorGUI::create_outside_buttons(unsigned floors_numbers, int lower, const QRect &form)
{
    gb_outside_buttons = create_buttons_block(this->parentWidget(), form, "gb_inside_buttons", "Buttons on floors");
    QRect form_up(0, 0, form.width() / 2, form.height());
    QRect form_down(form.width() / 2, 0, form.width() / 2, form.height());
    outside_buttons_up = create_buttons(gb_outside_buttons, floors_numbers, lower, form_up, outside_button_text_up);
    outside_buttons_down = create_buttons(gb_outside_buttons, floors_numbers, lower, form_down, outside_button_text_down);
}

ElevatorButton *ElevatorGUI::create_buttons(QWidget *parent, unsigned num, int lower, const QRect &form, btn_texter text)
{
    ElevatorButton *buttons = new ElevatorButton[num];
    int x = form.x() + int(form.width() * REL_HOR_BTN_SHIFT);
    int y = form.y() + int(form.height() * REL_VER_BTN_SHIFT);
    int w = int(form.width() * (1 - 2 * REL_HOR_BTN_SHIFT));
    int h = int(form.height() * (1 - (num + 1) * REL_VER_BTN_SHIFT) / num);
    int h_total = h + y;
    for (unsigned i = 0; i < num; ++i)
    {
        buttons[i].setParent(parent);
        buttons[i].setGeometry(x, y + h_total * i, w, h);
        buttons[i].setText(text(i + lower));
    }
    return buttons;
}

QGroupBox *ElevatorGUI::create_buttons_block(QWidget *parent, const QRect &form, const QString &object_name, const QString &title)
{
    QFont gb_font;
    gb_font.setPointSize(12);
    QGroupBox *gb = new QGroupBox;
    gb->setObjectName(object_name);
    gb->setParent(parent);
    gb->setGeometry(form);
    gb->setTitle(title);
    gb->setFont(gb_font);
    gb->setAlignment(Qt::AlignCenter);
    return gb;
}
