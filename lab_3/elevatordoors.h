#ifndef ELEVATORDOORS_H
#define ELEVATORDOORS_H

#include <QObject>
#include <QTimer>

class ElevatorDoors: public QObject
{
    Q_OBJECT
public:
    ElevatorDoors(QObject *parent = nullptr);
    void log();

signals:
    void doors_opened();
    void doors_closed();
    void log_request(QString);

public slots:
    void handler_opening();
    void handler_closing();

private:
    enum DoorsState {OPENED, CLOSED, OPENING, CLOSING} state;
    QTimer timer_opening, timer_closing, timer_standby; // Dumb but needed to'keep model'
    void change_state(DoorsState new_state);

private slots:
    void handler_opened();
    void handler_closed();
};

#endif // ELEVATORDOORS_H
