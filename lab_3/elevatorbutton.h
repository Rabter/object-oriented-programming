#ifndef ELEVATORBUTTON_H
#define ELEVATORBUTTON_H

#include <QPushButton>
#include "direction.h"

class ElevatorButton : public QPushButton
{
    Q_OBJECT
public:
    explicit ElevatorButton(QWidget *parent = nullptr);
    void set_floor_index(unsigned index);
    void set_direction(Direction dir);

signals:
    void clicked_at(unsigned floor_index, Direction dir);
    void log_request(QString txt);

private:
    unsigned floor_index;
    Direction dir;

    void log();

private slots:
    void handle_click();
};

#endif // ELEVATORBUTTON_H
