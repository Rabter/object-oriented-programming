#include "elevatorcontroller.h"

ElevatorController::ElevatorController(QObject *parent, unsigned floors_number, unsigned start_floor): QObject(parent), floors_number(floors_number)
{
    dir = NONE;
    state = SLEEPING;
    current_floor = start_floor;
    highest_call = 0;
    lowest_call = floors_number;
    stops[0] = new bool[floors_number];
    stops[1] = new bool[floors_number];
    for (unsigned i = 0; i < floors_number; ++i)
        stops[0][i] = stops[1][i] = false;
}

void ElevatorController::stop_at(unsigned floor_index, Direction dir)
{
    if (floor_index != current_floor)
    {
        if (floor_index > highest_call)
            highest_call = floor_index;
        if (floor_index < lowest_call)
            lowest_call = floor_index;
        if (dir < 2)
            stops[dir][floor_index] = true;
        else if (dir == BOTH)
            stops[UP][floor_index] = stops[DOWN][floor_index] = true;
    }
    if (state == SLEEPING)
    {
        state = WAITING;
        if (floor_index != current_floor)
            ask_to_move();
        else
            emit stop_now();
    }
}

void ElevatorController::achieved_floor(unsigned floor_index)
{
    if (state == MOVING && dir < 2)
    {
        current_floor = floor_index;
        if (current_floor == highest_call && dir == UP)
        {
            stops[UP][floor_index] = stops[DOWN][floor_index] = false;
            reset_boundary();
            if (check_bellow(current_floor))
            {
                state = WAITING;
                this->dir = DOWN;
            }
            else
            {
                this->dir = NONE;
                state = SLEEPING;
            }
            emit stop_now();
        }
        else if (current_floor == lowest_call && dir == DOWN)
        {
            stops[UP][floor_index] = stops[DOWN][floor_index] = false;
            reset_boundary();
            if (check_above(current_floor))
            {
                state = WAITING;
                this->dir = UP;
            }
            else
            {
                this->dir = NONE;
                state = SLEEPING;
            }
            emit stop_now();
        }
        else
        {
            if (stops[dir][floor_index])
            {
                state = WAITING;
                stops[UP][floor_index] = false;
                stops[DOWN][floor_index] = false;
                reset_boundary();
                emit stop_now();
            }
            else
            {
                state = WAITING;
                reset_boundary();
                ask_to_move();
            }
        }
    }
}

void ElevatorController::ask_to_move()
{
    if (state == WAITING)
    {
        if (dir == NONE)
        {
            if (check_above(current_floor))
            {
                dir = UP;
                state = MOVING;
                emit move_up();
            }
            else if (check_bellow(current_floor))
            {
                dir = DOWN;
                state = MOVING;
                emit move_down();
            }
        }
        else if (dir == UP)
            emit move_up();
        else
            emit move_down();
        state = MOVING;
    }
}

bool ElevatorController::check_above(unsigned floor)
{
    for (unsigned i = floor + 1; i < floors_number; ++i)
        if (stops[UP][i] || stops[DOWN][i])
            return true;
    return false;
}

bool ElevatorController::check_bellow(unsigned floor)
{
    for (long i = long(floor) - 1; i >= 0; --i)
        if (stops[UP][i] || stops[DOWN][i])
            return true;
    return false;
}

void ElevatorController::reset_highest()
{
    highest_call = 0;
    for (unsigned i = 1; i < floors_number; ++i)
        if (stops[UP][i] || stops[DOWN][i])
            highest_call = i;
}

void ElevatorController::reset_lowest()
{
    if (stops[UP][0] || stops[DOWN][0])
        lowest_call = 0;
    else
    {
        lowest_call = floors_number;
        for (unsigned i = floors_number - 2; i != 0; --i)
            if (stops[UP][i] || stops[DOWN][i])
                lowest_call = i;
    }
}

void ElevatorController::reset_boundary()
{
    if (current_floor == highest_call)
        reset_highest();
    if (current_floor == lowest_call)
        reset_lowest();
}

