#ifndef ELEVATOR_H
#define ELEVATOR_H

#include <QWidget>
#include <QTextEdit>
#include "elevatorgui.h"
#include "elevatorcabin.h"
#include "elevatordoors.h"
#include "elevatorcontroller.h"
#include "elevatorgui.h"
#include "elevator_defaults.h"

class Elevator: public QWidget
{
    Q_OBJECT
public:
    explicit Elevator(QWidget *parent = nullptr, unsigned floors_number = DEFAULT_FLOORS_NUMBER, unsigned start = DEFAULT_START_FLOOR, int lower = DEFAULT_LOWER_NUMBER);
    bool set_floors_number(unsigned num);
    bool set_lower_floor_number(int i);
    void set_logger(QTextEdit *logger, bool log_now = true);
    void draw_visuals();

    unsigned get_floors_number();

protected:
    ElevatorCabin *cabin;
    ElevatorDoors *doors;
    ElevatorController *controller;
    ElevatorGUI *visual;
    QTextEdit *logger;

protected slots:
    void log_all();
    void log(QString text);

private:
    unsigned floors_number;
    int lower;
    bool unlocked; // Locks after drawing
};

#endif // ELEVATOR_H
