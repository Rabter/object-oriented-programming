#include <QString>
#include "elevatordoors.h"
#include "time_intervals.h"

ElevatorDoors::ElevatorDoors(QObject *parent): QObject(parent)
{
    change_state(CLOSED);
    connect(&(this->timer_opening), SIGNAL(timeout()), this, SLOT(handler_opened()));
    connect(&(this->timer_closing), SIGNAL(timeout()), this, SLOT(handler_closed()));
    connect(&(this->timer_standby), SIGNAL(timeout()), this, SLOT(handler_closing()));
    timer_opening.setSingleShot(true);
    timer_closing.setSingleShot(true);
    timer_standby.setSingleShot(true);
}

void ElevatorDoors::handler_opening()
{
    if (state == CLOSED)
    {
        change_state(OPENING);
        timer_opening.start(DOORS_OPENING_TIME);
    }
    else if (state == CLOSING)
    {
        change_state(OPENING);
        timer_opening.start(timer_opening.remainingTime());
    }
}

void ElevatorDoors::handler_closing()
{
    if (state == OPENED)
    {
        change_state(CLOSING);
        timer_closing.start(DOORS_CLOSING_TIME);
    }
    else if (state == OPENING)
    {
        change_state(CLOSING);
        timer_closing.start(timer_opening.remainingTime());
    }
}

void ElevatorDoors::handler_opened()
{
    if (state == OPENING)
    {
        change_state(OPENED);
        timer_standby.start(CABIN_OPENED_STANDBY_TIME);
        emit doors_opened();
    }
}

void ElevatorDoors::handler_closed()
{
    if (state == CLOSING)
    {
        change_state(CLOSED);
        emit doors_closed();
    }
}

void ElevatorDoors::change_state(ElevatorDoors::DoorsState new_state)
{
    if (state != new_state)
    {
        state = new_state;
        log();
    }
}

void ElevatorDoors::log()
{
    QString state_str("Doors are ");
    if (state == OPENING)
        state_str += "opening";
    else if (state == CLOSING)
        state_str += "closing";
    else if (state == OPENED)
        state_str += "opened";
    else if (state == CLOSED)
        state_str += "closed";
    emit log_request(state_str);

}
