#include "elevator.h"

Elevator::Elevator(QWidget *parent, unsigned floors_number, unsigned start, int lower): QWidget(parent)
{
    unlocked = true; // Visuals will be drew after all parameters are set (they can be set outside the constructor)
    this->floors_number = floors_number;
    this->lower = lower;
    logger = nullptr;
    controller = new ElevatorController(this, floors_number, start);
    cabin = new ElevatorCabin(this, start);
    doors = new ElevatorDoors(this);

    connect(doors, SIGNAL(doors_closed()), controller, SLOT(ask_to_move()));

    connect(doors, SIGNAL(log_request(QString)), this, SLOT(log(QString)));

    connect(cabin, SIGNAL(opening_request()), doors, SLOT(handler_opening()));
    connect(cabin, SIGNAL(closing_request()), doors, SLOT(handler_closing()));

    connect(cabin, SIGNAL(visited_floor(uint)), controller, SLOT(achieved_floor(uint)));
    connect(cabin, SIGNAL(log_request(QString)), this, SLOT(log(QString)));

    connect(controller, SIGNAL(stop_now()), cabin, SLOT(stop()));
    connect(controller, SIGNAL(move_up()), cabin, SLOT(move_up()));
    connect(controller, SIGNAL(move_down()), cabin, SLOT(move_down()));
}

bool Elevator::set_floors_number(unsigned num)
{
    if (unlocked)
        floors_number = num;
    return unlocked;
}

bool Elevator::set_lower_floor_number(int i)
{
    if (unlocked)
        lower = i;
    return unlocked;
}

void Elevator::set_logger(QTextEdit *logger, bool log_now)
{
    this->logger = logger;
    if (log_now)
        emit log_all();
}

void Elevator::draw_visuals()
{
    if (unlocked)
    {
        unlocked = false;
        visual = new ElevatorGUI(this, floors_number, lower);
        ElevatorButton *inside = visual->get_inside_buttons();
        ElevatorButton *outside_up = visual->get_outside_buttons_up();
        ElevatorButton *outside_down = visual->get_outside_buttons_down();
        for (unsigned i = 0; i < floors_number; ++i)
        {
            inside[i].set_floor_index(i);
            inside[i].set_direction(BOTH);
            outside_up[i].set_floor_index(i);
            outside_up[i].set_direction(UP);
            outside_down[i].set_floor_index(i);
            outside_down[i].set_direction(DOWN);
            connect(inside + i, SIGNAL(clicked_at(uint, Direction)), controller, SLOT(stop_at(uint, Direction)));
            connect(outside_up + i, SIGNAL(clicked_at(uint, Direction)), controller, SLOT(stop_at(uint, Direction)));
            connect(outside_down + i, SIGNAL(clicked_at(uint, Direction)), controller, SLOT(stop_at(uint, Direction)));

            connect(inside + i, SIGNAL(log_request(QString)), this, SLOT(log(QString)));
            connect(outside_up + i, SIGNAL(log_request(QString)), this, SLOT(log(QString)));
            connect(outside_down + i, SIGNAL(log_request(QString)), this, SLOT(log(QString)));
        }
        connect(cabin, SIGNAL(stopped_at(uint)), visual, SLOT(activate(uint)));
    }
}

unsigned Elevator::get_floors_number()
{
    return floors_number;
}

void Elevator::log_all()
{
    doors->log();
    cabin->log_all();
}

void Elevator::log(QString text)
{
    if (logger)
        logger->append(text);
}
