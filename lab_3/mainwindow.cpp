#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->te_logger->setReadOnly(true);
    //ui->elevator_widget->set_lower_floor_number(-1);
    ui->elevator_widget->set_logger(ui->te_logger);
    ui->elevator_widget->draw_visuals();
}

MainWindow::~MainWindow()
{
    delete ui;
}
