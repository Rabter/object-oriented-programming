#include <sstream>
#include <string>
#include "int_to_string.h"

QString to_qstring(int val)
{
    std::ostringstream oss;
    oss << val;
    return QString(oss.str().c_str());
}
