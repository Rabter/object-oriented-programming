#ifndef ELEVATORGUI_H
#define ELEVATORGUI_H

#include <QGroupBox>
#include <QWidget>
#include "elevatorbutton.h"
#include "elevator_defaults.h"

typedef QString (*btn_texter)(int i);

class ElevatorGUI : public QWidget
{
    Q_OBJECT
public:
    explicit ElevatorGUI(QWidget *parent, unsigned floors_number = DEFAULT_FLOORS_NUMBER, int lower = DEFAULT_LOWER_NUMBER);
    ElevatorButton *get_inside_buttons();
    ElevatorButton *get_outside_buttons_up();
    ElevatorButton *get_outside_buttons_down();

public slots:
    void activate(unsigned index);

protected:
    ElevatorButton *inside_buttons;
    ElevatorButton *outside_buttons_up;
    ElevatorButton *outside_buttons_down;

    void create_inside_buttons(unsigned floors_numbers, int lower, const QRect &form);
    void create_outside_buttons(unsigned floors_numbers, int lower, const QRect &form);
private:
    QGroupBox *gb_inside_buttons, *gb_outside_buttons;
    ElevatorButton *create_buttons(QWidget *parent, unsigned num, int lower, const QRect &form, btn_texter text);
    QGroupBox *create_buttons_block(QWidget *parent, const QRect &form, const QString &object_name, const QString &title);
};

#endif // ELEVATORGUI_H
