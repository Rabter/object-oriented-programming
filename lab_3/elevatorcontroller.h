#ifndef ELEVATORCONTROLLER_H
#define ELEVATORCONTROLLER_H

#include <QObject>
#include "elevator_defaults.h"
#include "direction.h"

class ElevatorController: public QObject
{
    Q_OBJECT
public:
    explicit ElevatorController(QObject *parent = nullptr, unsigned floors_number = DEFAULT_FLOORS_NUMBER, unsigned start_floor = DEFAULT_START_FLOOR);

signals:
    void stop_now();
    void move_up();
    void move_down();

public slots:
    void stop_at(unsigned floor_index, Direction dir);
    void achieved_floor(unsigned floor_index);
    void ask_to_move();

private:
    Direction dir;
    enum ControllerState {SLEEPING, WAITING, MOVING} state;
    const unsigned floors_number;
    unsigned current_floor;
    unsigned highest_call, lowest_call;
    bool *stops[2];

    bool check_above(unsigned floor);
    bool check_bellow(unsigned floor);
    void reset_highest();
    void reset_lowest();
    void reset_boundary();
};

#endif // ELEVATORCONTROLLER_H
