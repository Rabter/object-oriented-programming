#ifndef ELEVATORCABIN_H
#define ELEVATORCABIN_H

#include <QObject>
#include <QTimer>
#include <QString>
#include "elevator_defaults.h"
#include "direction.h"

class ElevatorCabin: public QObject
{
    Q_OBJECT
public:
    ElevatorCabin(QObject *parent = nullptr, unsigned floor = DEFAULT_START_FLOOR);
    void log_all();

public slots:
    void stop();
    void move_up();
    void move_down();

signals:
    void opening_request();
    void closing_request();
    void visited_floor(unsigned floor_index);
    void stopped_at(unsigned floor_index);
    void log_request(QString);

private:
    enum CabinState{STILL, MOVING_UP, MOVING_DOWN, EXPECTING_COMMAND} state;
    QTimer timer;
    unsigned floor;

    void change_state(CabinState new_state);
    void log_state();
    void log_floor();
    void next_floor();

private slots:
    void expect();
};

#endif // ELEVATORCABIN_H
