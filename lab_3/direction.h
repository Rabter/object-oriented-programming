#ifndef DIRECTION_H
#define DIRECTION_H

enum Direction {UP = 0, DOWN = 1, BOTH = 2, NONE = 3};

#endif // DIRECTION_H
