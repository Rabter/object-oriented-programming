#include <QString>
#include "int_to_string.h"
#include "elevatorbutton.h"

ElevatorButton::ElevatorButton(QWidget *parent) : QPushButton(parent)
{
    connect(this, SIGNAL(clicked()), this, SLOT(handle_click()));
}

void ElevatorButton::set_floor_index(unsigned index)
{
    floor_index = index;
}

void ElevatorButton::set_direction(Direction dir)
{
    this->dir = dir;
}

void ElevatorButton::log()
{
    QString txt;
    if (dir == BOTH)
        txt = "Pressed inside button ";
    else if (dir == UP)
            txt = "Pressed outside button ^";
    else if (dir == DOWN)
            txt = "Pressed outside button v";
    txt += to_qstring(floor_index + 1);
    emit log_request(txt);
}

void ElevatorButton::handle_click()
{
    log();
    setDisabled(true);
    emit clicked_at(floor_index, dir);
}
