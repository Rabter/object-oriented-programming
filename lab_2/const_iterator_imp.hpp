#ifndef CONST_ITERATOR_IMP_HPP
#define CONST_ITERATOR_IMP_HPP

#include "const_iterator.hpp"

template <class T>
ConstIterator<T>::ConstIterator(): BaseIterator<T>() {}

template <class T>
ConstIterator<T>::ConstIterator(const std::shared_ptr<T> &address, size_t size, size_t index): BaseIterator<T>(address, size, index) {}

template <class T>
ConstIterator<T>::ConstIterator (const ConstIterator<T> &other): BaseIterator<T>(other) {}

template <class T>
const T* ConstIterator<T>::where() const
{
    return this->actual_address();
}

template <class T>
const T& ConstIterator<T>::operator *()
{
    return *(this->actual_address());
}

#endif // CONST_ITERATOR_IMP_HPP
