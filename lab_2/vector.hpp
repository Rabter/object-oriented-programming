#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <iostream>
#include <memory>
#include "container.h"
#include "iterator.hpp"
#include "const_iterator.hpp"

template <class T>
class Vector: public Container
{
public:
    Vector();
    explicit Vector(const Vector<T> &other);
    Vector(size_t init_dimension);
    Vector(size_t init_dimension, const T& value);
    Vector(size_t init_dimension, const Vector<T> &other);
    Vector(size_t init_dimension, const T* arr);
    Vector(Vector<T> &&other);
    Vector(const std::initializer_list<T> &list);

    Iterator<T> begin();
    Iterator<T> end();
    ConstIterator<T> begin() const;
    ConstIterator<T> end() const;

    void resize(size_t new_size);

    T& at(size_t index);
    const T& at(size_t index) const;

    Vector<T>& clone_from(const Vector<T> &other);
    Vector<T>& clone_from(Vector<T> &&other);
    Vector<T>& clone_from(const std::initializer_list<T> list);

    Vector<T> multiply(const T &scalar);
    Vector<T> multiply(const Vector<T> &other);
    Vector<T> subtract(const Vector<T> &other);
    Vector<T> add(const Vector<T> &other);
    Vector<T> reverse();

    Vector<T> multiplication(const T &scalar) const;
    T scalar_product(const Vector<T> &other) const;
    Vector<T> vector_product(const Vector<T> &other) const;
    Vector<T> difference(const Vector<T> &other) const;
    Vector<T> sum(const Vector<T> &other) const;
    Vector<T> negative() const;

    bool is_equal(const Vector<T> &other) const;

    T& operator [] (size_t index);
    const T& operator [](size_t index) const;
    T& operator () (size_t index);
    const T& operator ()(size_t index) const;

    Vector<T>& operator = (const Vector<T> &other);
    Vector<T>& operator = (Vector<T> &&other);
    Vector<T>& operator = (const std::initializer_list<T> &list);

    Vector<T> operator ^= (const Vector<T> &other);
    Vector<T> operator += (const Vector<T> &other);
    Vector<T> operator -= (const Vector<T> &other);

    template <class C> friend Vector<C> operator * (const Vector<C> &a, const T& scalar);
    template <class C> friend C operator * (const Vector<C> &a, const Vector<C> &b); // Scalar product
    template <class C> friend Vector<C> operator ^ (const Vector<C> &a, const Vector<C> &b); // Vector product
    template <class C> friend Vector<C> operator + (const Vector<C> &a, const Vector<C> &b);
    template <class C> friend Vector<C> operator - (const Vector<C> &a, const Vector<C> &b);
    template <class C> friend Vector<C> operator - (const Vector<C> &a);

    template <class C> friend bool operator == (const Vector<C> &a, const Vector<C> &b);

    template <class C> friend std::ostream& operator << (std::ostream &stout, const Vector<C> &output);

protected:
    std::shared_ptr<T> data;
};

#include "vector_imp.hpp"

#endif // VECTOR_HPP
