#include <iostream>
#include "vector.hpp"
using namespace std;

int main()
{
    Vector<int> vec1 = {1, 0, 2};
    Vector<int> vec2(vec1);
    Vector<int> vec3(3);
    vec3 = {0, 1, 1};
    cout << vec1 << endl << vec2 << endl;
    cout << vec1 * vec3 << endl;
    cout << (vec1 ^ vec3) << endl; // Parenthesses are important here
    cout << vec1 + vec3 << endl;
    cout << vec1 - vec3 << endl;
    cout << -vec2 << endl;
    BaseException a("",time(NULL),1);
    cout << a << endl;
    Vector<int> vec4(4, 0);
    try
    {
        vec1 ^ vec4;
    }
    catch (WrongInputException exc)
    {
        cout << endl << exc << endl;
    }
}
