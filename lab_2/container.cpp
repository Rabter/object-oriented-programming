#include "container.h"


Container::Container()
{
    _size = 0;
}

Container::Container(size_t size)
{
    _size = size;
}

size_t Container::size() const
{
    return _size;
}

bool Container::is_empty() const
{
    return _size == 0;
}
