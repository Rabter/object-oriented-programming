#ifndef CONST_ITERATOR_HPP
#define CONST_ITERATOR_HPP

#include "base_iterator.hpp"

template <class T>
class ConstIterator: public BaseIterator<T>
{
public:
    ConstIterator();
    ConstIterator(const std::shared_ptr<T> &address, size_t size, size_t index = 0);
    ConstIterator(const ConstIterator &other);
    const T* where() const;
    const T& operator * ();
};

#include "const_iterator_imp.hpp"

#endif // CONST_ITERATOR_HPP
