#ifndef BASE_EXCEPTION_H
#define BASE_EXCEPTION_H
#include <exception>
#include <iostream>
#include <string>
#include <ctime>
using namespace std;

class BaseException: public std::exception
{
public:
    BaseException();
    BaseException(const string &messsage, time_t time = 0, unsigned line = 0);
    std::string string_what() const;
    const char* what() const noexcept;
    unsigned where() const;
    time_t when() const;

    friend std::ostream& operator << (std::ostream &stout, const BaseException &output);

protected:
    string msg;
    time_t time;
    unsigned line;
};

#endif // BASE_EXCEPTION_H
