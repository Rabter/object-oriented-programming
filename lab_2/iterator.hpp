#ifndef ITERATOR_HPP
#define ITERATOR_HPP

#include "base_iterator.hpp"

template <class T>
class Iterator: public BaseIterator<T>
{
public:
    Iterator();
    Iterator(const std::shared_ptr<T> &address, size_t size, size_t index = 0);
    Iterator(Iterator &other);
    T* where() const;
    T& operator * ();
};

#include "iterator_imp.hpp"

#endif // ITERATOR_HPP
