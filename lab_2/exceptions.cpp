#include "exceptions.h"

WrongInputException::WrongInputException(): BaseException() {}

WrongInputException::WrongInputException(const string &messsage, time_t time, unsigned line):
    BaseException(messsage, time, line) {}

BaseException WrongInputException::to_base() const
{
    return BaseException(this->msg, this->time, this->line);
}

ostream& operator << (ostream &stout, const WrongInputException &output)
{
    stout << "Wrong input exception." << endl;
    stout << output.to_base();
    return stout;
}
