#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include "base_exception.h"

class WrongInputException: public BaseException
{
public:
    WrongInputException();
    WrongInputException(const string &messsage, time_t time = 0, unsigned line = 0);

    friend std::ostream& operator << (std::ostream &stout, const WrongInputException &output);

protected:
    BaseException to_base() const;
};

#endif // EXCEPTIONS_H
