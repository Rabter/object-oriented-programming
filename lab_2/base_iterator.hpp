#ifndef BASE_ITERATOR_HPP
#define BASE_ITERATOR_HPP

#include <memory>
#include <stddef.h>

template <class T>
class BaseIterator
{
public:
    BaseIterator() = default;
    BaseIterator(const std::shared_ptr<T> &address, size_t size, size_t index);
    BaseIterator(const BaseIterator<T> &other);

    bool check() const;

    BaseIterator& operator ++();
    BaseIterator& operator --();
    BaseIterator operator ++(int);
    BaseIterator operator --(int);
    ptrdiff_t operator -(const BaseIterator<T> &other) const;

    template <class C> friend bool operator == (const BaseIterator<C> &it1, const BaseIterator<C> &it2);
    template <class C> friend bool operator != (const BaseIterator<C> &it1, const BaseIterator<C> &it2);
    template <class C> friend bool operator > (const BaseIterator<C> &it1, const BaseIterator<C> &it2);
    template <class C> friend bool operator < (const BaseIterator<C> &it1, const BaseIterator<C> &it2);
    template <class C> friend bool operator >= (const BaseIterator<C> &it1, const BaseIterator<C> &it2);
    template <class C> friend bool operator <= (const BaseIterator<C> &it1, const BaseIterator<C> &it2);

protected:
    std::weak_ptr<T> address;
    size_t size, index;
    T* actual_address() const;
};

#include "base_iterator_imp.hpp"

#endif // BASE_ITERATOR_HPP
