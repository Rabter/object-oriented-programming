#ifndef _VECTOR_IMP_HPP_
#define _VECTOR_IMP_HPP_

#include <time.h>
#include "vector.hpp"
#include "iterator.hpp"
#include "const_iterator.hpp"
#include "exceptions.h"

template <class T>
Vector<T>::Vector(): Container() {}

template <class T>
Vector<T>::Vector(size_t init_dimension): Container(init_dimension)
{
    data = std::shared_ptr<T>(new T[_size], std::default_delete<T[]>());
}

template <class T>
Vector<T>::Vector(size_t init_dimension, const T& value): Vector(init_dimension)
{
    for (size_t i = 0; i < _size; ++i)
        (*this)[i] = value;
}

template <class T>
Vector<T>::Vector(size_t init_dimension, const Vector<T> &other): Vector(init_dimension)
{
    size_t min_size = std::min(init_dimension, other._size), i;
    for (i = 0; i < min_size; ++i)
        (*this)[i] = other[i];
    while (i < _size)
        (*this)[i++] = 0;

}

template <class T>
Vector<T>::Vector(size_t init_dimension, const T* arr): Vector(init_dimension)
{
    for (size_t i = 0; i < init_dimension; ++i)
        (*this)[i] = arr[i];
}

template <class T>
Vector<T>::Vector(const Vector<T>& other): Vector(other._size)
{
    for(size_t i = 0; i < _size; ++i)
        data.get()[i] = other.data.get()[i];
}

template <class T>
Vector<T>::Vector(Vector<T>&& other): Vector(other._size)
{
    data = other.data;
    other.data = nullptr;
    other._size = 0;
}

template <class T>
Vector<T>::Vector(const std::initializer_list<T> &list): Vector(list.size())
{
    std::copy(list.begin(), list.end(), this->data.get());
}

template <class T>
Iterator<T> Vector<T>::begin()
{
   return Iterator<T>(this->data, _size);
}

template <class T>
Iterator<T> Vector<T>::end()
{
   return Iterator<T>(this->data, _size, _size);
}

template <class T>
ConstIterator<T> Vector<T>::begin() const
{
   return ConstIterator<T>(this->data, _size);
}

template <class T>
ConstIterator<T> Vector<T>::end() const
{
   return ConstIterator<T>(this->data, _size, _size);
}

template <class T>
void Vector<T>::resize(size_t new_size)
{
    *this = Vector<T>(new_size, *this);
}

template <class T>
T& Vector<T>::at(size_t index)
{
    return data.get()[index];
}

template <class T>
const T& Vector<T>::at(size_t index) const
{
    return data.get()[index];
}

template <class T>
Vector<T>& Vector<T>::clone_from(const Vector<T> &other)
{
    return (*this) = other;
}

template <class T>
Vector<T>& Vector<T>::clone_from(Vector<T> &&other)
{
    return (*this) = other;
}

template <class T>
Vector<T>& Vector<T>::clone_from(const std::initializer_list<T> list)
{
    return (*this) = list;
}

template <class T>
Vector<T> Vector<T>::multiply(const T &scalar)
{
    return (*this) *= scalar;
}

template <class T>
Vector<T> Vector<T>::multiply(const Vector<T> &other)
{
    return (*this) ^= other;
}

template <class T>
Vector<T> Vector<T>::subtract(const Vector<T> &other)
{
    return (*this) -= other;
}

template <class T>
Vector<T> Vector<T>::add(const Vector<T> &other)
{
    return (*this) += other;
}

template <class T>
Vector<T> Vector<T>::reverse()
{
    return (*this) = -(*this);
}

template <class T>
Vector<T> Vector<T>::multiplication(const T &scalar) const
{
    return (*this) * scalar;
}

template <class T>
T Vector<T>::scalar_product(const Vector<T> &other) const
{
    return (*this) * other;
}

template <class T>
Vector<T> Vector<T>::vector_product(const Vector<T> &other) const
{
    return (*this) ^ other;
}

template <class T>
Vector<T> Vector<T>::difference(const Vector<T> &other) const
{
    return (*this) - other;
}

template <class T>
Vector<T> Vector<T>::sum(const Vector<T> &other) const
{
    return (*this) + other;
}

template <class T>
Vector<T> Vector<T>::negative() const
{
    return -(*this);
}

template <class T>
bool Vector<T>::is_equal(const Vector<T> &other) const
{
    return (*this) == other;
}

template <class T>
T& Vector<T>::operator [] (size_t index)
{
    return data.get()[index];
}

template <class T>
const T& Vector<T>::operator [] (size_t index) const
{
    return data.get()[index];
}

template <class T>
T& Vector<T>::operator () (size_t index)
{
    return data.get()[index];
}

template <class T>
const T& Vector<T>::operator () (size_t index) const
{
    return data.get()[index];
}

template <class T>
Vector<T>& Vector<T>::operator = (const Vector<T> &other)
{
    _size = other._size;
    data = std::shared_ptr<T>(new T[_size], std::default_delete<T[]>());
    for(size_t i = 0; i < _size; ++i)
        data.get()[i] = other.data.get()[i];
    return *this;
}

template <class T>
Vector<T>& Vector<T>::operator = (Vector<T> &&other)
{
    _size = other._size;
    data = other.data;
    other.data = nullptr;
    return *this;
}

template <class T>
Vector<T> &Vector<T>::operator = (const std::initializer_list<T> &list)
{
    data = std::shared_ptr<T>(new T[_size], std::default_delete<T[]>());
    _size = list.size();
    std::copy(list.begin(), list.end(), this->data.get());
    return *this;
}

template <class T>
Vector<T> Vector<T>::operator ^= (const Vector<T> &other)
{
    //Multiplication of TWO vectors is only possible in three-dimensional space
    if (_size != 3 || other.size() != 3)
    {
        time_t t_time = time(NULL);
        throw WrongInputException("At least one vector is not 3-dimensional",  time(&t_time), 153);
    }

    T tmp[3] = {(*this)[0], (*this)[1], (*this)[2]};
    (*this)[0] = tmp[1] * other[2] - tmp[2] * other[1];
    (*this)[1] = tmp[2] * other[0] - tmp[0] * other[2];
    (*this)[2] = tmp[0] * other[1] - tmp[1] * other[0];

    return *this;
}

template <class T>
Vector<T> Vector<T>::operator += (const Vector<T> &other)
{
    size_t other_size = other.size();
    if (_size < other_size)
        resize(other_size);
    for (size_t i = 0; i < other_size; ++i)
        (*this)[i] += other[i];
    return *this;
}

template <class T>
Vector<T> Vector<T>::operator -= (const Vector<T> &other)
{
    size_t other_size = other.size();
    if (_size < other_size)
        resize(other_size);
    for (size_t i = 0; i < other_size; ++i)
        (*this)[i] -= other[i];
    return *this;
}

template <class C>
Vector<C> operator * (const Vector<C> &a, const C &scalar)
{
    Vector<C> result(a);
    for (size_t i = 0; i < result._size; ++i)
        result[i] = a[i] * scalar;
    return result;
}

template <class C>
C operator * (const Vector<C> &a, const Vector<C> &b)
{
    ConstIterator<C> it1, it2, end;
    if (a._size > b._size)
    {
        it1 = b.begin();
        it2 = a.begin();
        end = b.end();
    }
    else
    {
        it1 = a.begin();
        it2 = b.begin();
        end = a.end();
    }
    C sum = 0;
    while (it1 != end)
    {
        sum += *it1 * *it2;
        ++it1;
        ++it2;
    }
    return sum;
}

template <class C>
Vector<C> operator ^ (const Vector<C> &a, const Vector<C> &b)
{
    //Multiplication of TWO vectors is only possible in three-dimensional space
    if (a._size != 3 || b._size != 3)
    {
        time_t t_time = time(NULL);
        throw WrongInputException("At least one vector is not 3-dimensional",  time(&t_time), 153);
    }
    Vector<C> result(a);
    result[0] = a[1] * b[2] - a[2] * b[1];
    result[1] = a[2] * b[0] - a[0] * b[2];
    result[2] = a[0] * b[1] - a[1] * b[0];

    return result;
}

template <class C>
Vector<C> operator + (const Vector<C> &a, const Vector<C> &b)
{
    Vector<C> result;
    size_t a_size = a.size(), b_size = b.size();
    if (a_size > b_size)
    {
        result = Vector<C>(a);
        for (size_t i = 0; i < b_size; ++i)
            result[i] += b[i];
    }
    else
    {
        result = Vector<C>(b);
        for (size_t i = 0; i < a_size; ++i)
            result[i] += a[i];
    }
    return result;
}

template <class C>
Vector<C> operator - (const Vector<C> &a, const Vector<C> &b)
{
    Vector<C> result;
    size_t a_size = a.size(), b_size = b.size();
    if (a_size > b_size)
    {
        result = Vector<C>(a);
        for (size_t i = 0; i < b_size; ++i)
            result[i] -= b[i];
    }
    else
    {
        result = -Vector<C>(b);
        for (size_t i = 0; i < a_size; ++i)
            result[i] += a[i];
    }
    return result;
}

template <class C>
Vector<C> operator - (const Vector<C> &a)
{
    size_t a_size = a.size();
    Vector<C> result(a_size);
    for (size_t i = 0; i < a_size; ++i)
        result[i] = -a[i];
    return result;
}


template <class C>
bool operator == (const Vector<C> &a, const Vector<C> &b)
{
    if (a._size != b._size)
        return false;
    for (size_t i = 0; i < a._size; ++i)
        if (a[i] != b[i])
            return false;
    return true;
}

template <class C>
std::ostream& operator << (std::ostream &stout, const Vector<C> &output)
{
    stout << '(';
    ConstIterator<C> current = output.begin(), end = output.end();
    --end;
    while (current != end)
    {
        stout << *current << ", ";
        ++current;
    }
    stout << *current << ')';
    return stout;
}

#endif
