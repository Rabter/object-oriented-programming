#include "base_exception.h"
#define NO_ERR_INFO_TEXT "An unknown error occurred"
#define TIME_BUFFER_SIZE 30

BaseException::BaseException()
{
    this->msg = "";
    this->time = 0;
    this->line = 0;
}

BaseException::BaseException(const string &messsage, time_t time, unsigned line)
{
    this->msg = messsage;
    this->time = time;
    this->line = line;
}

string BaseException::string_what() const
{
    if (msg == "")
        return NO_ERR_INFO_TEXT;
    return
        msg;
}

const char* BaseException::what() const noexcept
{
    if (msg == "")
        return NO_ERR_INFO_TEXT;
    return
        msg.c_str();
}
unsigned BaseException::where() const
{
    return line;
}

time_t BaseException::when() const
{
    return time;
}

std::ostream& operator << (std::ostream &stout, const BaseException &output)
{
    stout << "Error text: " << (output.msg == ""? "unknown" : output.msg) << endl;
    stout << "Error time is ";
    char buf[TIME_BUFFER_SIZE + 1] = "unknown";
    if (output.time)
        std::strftime(buf, TIME_BUFFER_SIZE, "%c", std::localtime(&(output.time)));
    stout << buf << endl;
    stout << "Error line is ";
    if (output.line == 0)
        stout << "unknown";
    else
        stout << output.line;
    return stout;
}

#undef NO_ERR_INFO_TEXT
#undef TIME_BUFFER_SIZE
