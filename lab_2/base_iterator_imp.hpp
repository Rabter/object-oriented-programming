#ifndef BASE_ITERATOR_IMP_HPP
#define BASE_ITERATOR_IMP_HPP

#include "base_iterator.hpp"

template <class T>
BaseIterator<T>::BaseIterator(const std::shared_ptr<T> &address, size_t size, size_t index)
{
    this->address = address;
    this->size = size;
    this->index = index;
}

template <class T>
BaseIterator<T>::BaseIterator(const BaseIterator<T> &other)
{
    address = other.address;
    size = other.size;
    index = other.index;
}

template <class T>
bool BaseIterator<T>::check() const
{
    return !address.expired() && index >=0 && index < size;
}

template <class T>
BaseIterator<T>& BaseIterator<T>::operator ++()
{
    ++index;
    return *this;
}

template <class T>
BaseIterator<T>& BaseIterator<T>::operator --()
{
    --index;
    return *this;
}

template <class T>
BaseIterator<T> BaseIterator<T>::operator ++(int)
{
    BaseIterator tmp(*this);
    ++index;
    return tmp;
}

template <class T>
BaseIterator<T> BaseIterator<T>::operator --(int)
{
    BaseIterator tmp(*this);
    --index;
    return tmp;
}

template <class T>
ptrdiff_t BaseIterator<T>::operator -(const BaseIterator<T> &other) const
{
    return actual_address() - other.actual_address();
}

template <class C>
bool operator == (const BaseIterator<C>& it1, const BaseIterator<C>& it2)
{
    return it1.actual_address() == it2.actual_address();
}

template <class C>
bool operator != (const BaseIterator<C>& it1, const BaseIterator<C>& it2)
{
    return it1.actual_address() != it2.actual_address();
}

template <class C>
bool operator > (const BaseIterator<C> &it1, const BaseIterator<C> &it2)
{
    return it1.actual_address() > it2.actual_address();
}

template <class C>
bool operator < (const BaseIterator<C> &it1, const BaseIterator<C> &it2)
{
    return it1.actual_address() < it2.actual_address();
}

template <class C>
bool operator >= (const BaseIterator<C> &it1, const BaseIterator<C> &it2)
{
    return it1.actual_address() >= it2.actual_address();
}

template <class C>
bool operator <= (const BaseIterator<C> &it1, const BaseIterator<C> &it2)
{
    return it1.actual_address() <= it2.actual_address();
}

template <class T>
T* BaseIterator<T>::actual_address() const
{
    T* deb = address.lock().get() + index;
    return deb;
}

#endif // BASE_ITERATOR_IMP_HPP
