#ifndef CONTAINER_H
#define CONTAINER_H

#include <iostream>

class Container
{
public:
    Container();
    explicit Container(size_t size);

    size_t size() const;
    bool is_empty() const;

protected:
    size_t _size;
};

#endif // CONTAINER_H
