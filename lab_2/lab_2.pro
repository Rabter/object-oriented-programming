TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        base_exception.cpp \
        main.cpp \
    container.cpp \
    exceptions.cpp

HEADERS += \
    base_exception.h \
    vector.hpp \
    vector_imp.hpp \
    base_iterator.hpp \
    base_iterator_imp.hpp \
    iterator.hpp \
    const_iterator.hpp \
    iterator_imp.hpp \
    const_iterator_imp.hpp \
    container.h \
    exceptions.h
