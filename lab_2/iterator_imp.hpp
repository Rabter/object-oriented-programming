#ifndef ITERATOR_IMP_HPP
#define ITERATOR_IMP_HPP

#include "iterator.hpp"

template <class T>
Iterator<T>::Iterator(): BaseIterator<T>() {}
template <class T>
Iterator<T>::Iterator(const std::shared_ptr<T> &address, size_t size, size_t index): BaseIterator<T>(address, size, index) {}
template <class T>
Iterator<T>::Iterator(Iterator &other): BaseIterator<T>(other) {}

template <class T>
T* Iterator<T>::where() const
{
    return this->actual_address();
}

template <class T>
T& Iterator<T>::operator *()
{
    return *(this->actual_address());
}

#endif // ITERATOR_IMP_HPP
